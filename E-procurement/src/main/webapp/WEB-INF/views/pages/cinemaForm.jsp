<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 

<!--for displaying data content-->
<div class="row">

	<div class="col-md-3"></div>
	<div class="col-md-6 login-div">
		<div class="row login-div-name">
			<div class="col-xs-8">${ pageTitle }</div>
			<div class="col-xs-4 text-right"><a href="index.do?page=1"><button class="btn btn-primary"><i class="fas fa-arrow-left"></i> Back</button></a></div>
		</div>
		<br>
		<div class="row login-div-content card pb-3" style="background-color:#fff">
		  <form:form modelAttribute="cinemaDTO" method="POST">
			<div class="col-sm-12">
			<form:hidden path="id"/>
			     <c:if test = "${not empty errorMessage}">
			     <br>
			     <div class="alert alert-danger" role="alert">
                  <i class="fa fa-exclamation-circle" aria-hidden="true"></i> ${errorMessage}
                 </div>
                 </c:if>
				<br>
				<div class="row form-group">
					<div class="col-sm-4 label2"><label>Cinema Name</label><label class="label-red">*</label></div>
					<div class="col-sm-8">
							<form:input type="text" class="form-control" path="name" placeholder="Cinema Name" />
							<form:errors path="name" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>Address</label></div>
					<div class="col-sm-8"><form:input type="text" class="form-control" path="address" placeholder="Address" />
					<form:errors path="address" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>Phone No</label></div>
					<div class="col-sm-8"><form:input type="text" class="form-control" path="phno" placeholder="Phone No" />
					<form:errors path="phno" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>Details</label></div>
					<div class="col-sm-8"><form:textarea class="form-control" path="details" rows="5" placeholder="Details" />
					<form:errors path="details" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>Admin Json</label></div>
					<div class="col-sm-8"><form:textarea class="form-control" path="adminJson" rows="5" placeholder="Admin Json" />
					<form:errors path="adminJson" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>Database Url</label></div>
					<div class="col-sm-8"><form:textarea class="form-control" path="databaseURL" rows="5" placeholder="Database Url" />
					<form:errors path="databaseURL" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>Firebase InitializeApp Config</label></div>
					<div class="col-sm-8"><form:textarea class="form-control" path="firebaseInitializeAppConfig" rows="5" placeholder="Firebase InitializeApp Config" />
					<form:errors path="firebaseInitializeAppConfig" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<c:if test="${empty cinemaDTO.id}">
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>Core Admin UserName</label></div>
					<div class="col-sm-8"><form:input type="text" class="form-control" path="coreAdminUserName" placeholder="User Name" />
					<form:errors path="coreAdminUserName" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>Core Admin Password</label></div>
					<div class="col-sm-8"><form:input type="password" class="form-control" path="coreAdminPassword" placeholder="Password" />
					<form:errors path="coreAdminPassword" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				</c:if>
				<div>
                <form:radiobuttons items="${statusList}" path="status"  />
                </div><br>
				<div class=" d-grid gap-2">
						<form:button type="submit" class="btn btn-primary" value="insert">Save</form:button>
					
						<form:button type="reset" class="btn btn-danger">Clear</form:button>
				</div>
			</div>
		  </form:form>
		</div>
		<br>
	</div>
	<div class="col-md-3"></div>
</div>
<br>
<br>
<!-- end for displaying data content -->