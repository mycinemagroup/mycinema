<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-xxl-4 col-xl-5 col-lg-6 col-md-8">
			<div class="card card-raised shadow-10 mt-5 mt-xl-10 mb-4">
				<div class="card-body p-5">
					<!-- Auth header with logo image-->
					<div class="text-center">
						<img class="mb-3"
							src="https://material-admin-pro.startbootstrap.com/assets/img/icons/background.svg"
							alt="..." style="height: 48px">
						<h1 class="display-5 mb-0">Login</h1>
					</div>
					<!-- Login submission form-->
					<form action="doLogin" method="post">
						<div class="mb-4">
						<label class="form-label" for="username">User Name</label>
							<input class="form-control" placeholder="User Name" id="username"
								name='username' type="text" data-sb-validations="required">
						<div class="invalid-feedback" data-sb-feedback="username:required">UserName is required.</div>
						</div>
						<div class="mb-4">
						<label class="form-label" for="password">Password</label>
							<input class=" form-control" placeholder="Password"
								name='password' type="password" value="" data-sb-validations="required">
						<div class="invalid-feedback" data-sb-feedback="password:required">Password is required.</div>		
						</div>

						<div
							class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
							<a class="small fw-500 text-decoration-none"
								href="app-auth-password-basic.html">Forgot Password?</a> <input type="submit"
								class="btn btn-primary mdc-ripple-upgraded" value="Login" />
						</div>
					</form>
				</div>
			</div>
			<!-- Auth card message-->
			<div class="text-center mb-5">
				<a class="small fw-500 text-decoration-none link-white"
					href="app-auth-register-basic.html">Need an account? Sign up!</a>
			</div>
		</div>
	</div>
</div>