<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="row div-cover">
	<div class="col-md-12">
		<div class="row content-name">
			<div class="col-xs-8">${pageTitle}</div>
			<div class="col-xs-4 text-right pr-2">
				<a
					href="${pageContext.request.contextPath}/cinema/create"><button
						class="btn btn-light">
						<i class="fas fa-plus"></i> New
					</button></a>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<form:form modelAttribute="cinemaInquiryDTO" method="POST">
					<div class="row form-group">
						<div class="col-md-2 textbox-pointer-word">
							<form:input type="text" class="form-control" path="name"
								placeholder="Search By Name" />
							<br>
						</div>
						<div class="col-md-2">
							<form:button type="submit" class="btn btn-primary btn-block">Search</form:button>
						</div>
					</div>
				</form:form>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12 table-responsive card">
				<table class="table text-wrap table-bordered">
					<thead>
						<tr>
							<th>No</th>
							<th>Name</th>
							<th>Address</th>
							<th>Phone No</th>
							<th>details</th>
							<th>status</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${cinemaList}" var="cinema"
							varStatus="loop">
							<tr>
								<td>${((page-1)*numRow)+(loop.index+1)}</td>
								<td>${cinema.name }</td>
								<td>${cinema.address }</td>
								<td>${cinema.phno}</td>
								<td>${cinema.details }</td>
								<td>${cinema.status}</td>
								<td><a href="update?id=${cinema.id}"><i class="far fa-edit"></i> edit</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>