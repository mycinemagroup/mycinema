<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!--for displaying data content-->
<div class="row">

	<div class="col-md-3"></div>
	<div class="col-md-6 login-div">
		<div class="row login-div-name">
			<div class="col-xs-8">${ pageTitle }</div>
			<div class="col-xs-4 text-right">
				<a href="index.do?page=1"><button class="btn btn-primary">
						<i class="fas fa-arrow-left"></i> Back
					</button></a>
			</div>
		</div>
		<br>
		<div class="row login-div-content card pb-3"
			style="background-color: #fff">
			<form:form modelAttribute="userDTO" method="POST">
				<div class="col-sm-12">
					<form:hidden path="id" />
					<c:if test="${not empty errorMessage}">
						<br>
						<div class="alert alert-danger" role="alert">
							<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
							${errorMessage}
						</div>
					</c:if>
					<br>
					<div class="row form-group">
						<div class="col-sm-4 label2">
							<label>User Name</label><span class="label-danger">*</span>
						</div>
						<div class="col-sm-8">
							<form:input type="text" class="form-control" path="username"
								placeholder="User Name" />
							<form:errors path="username" cssClass="error" element="div"
								cssStyle="color : red ;" />
						</div>
					</div>
					<br>
					<div class="row form-group">
						<div class="col-sm-4 label1">
							<label>Password</label>
						</div>
						<div class="col-sm-8">
							<form:input type="password" class="form-control" path="password"
								placeholder="Password" />
							<form:errors path="password" cssClass="error" element="div"
								cssStyle="color : red ;" />
						</div>
					</div>
					<br>
					<div class="row form-group">
						<div class="col-sm-4 label1">
							<label>Confirm Password</label>
						</div>
						<div class="col-sm-8">
							<form:input type="password" class="form-control"
								path="confirmpassword" placeholder="Confirm Password" />
							<form:errors path="confirmpassword" cssClass="error"
								element="div" cssStyle="color : red ;" />
						</div>
					</div>
					<br>
					<div class="row form-group">
						<div class="col-sm-4 label1">
							<label>User Role</label>
						</div>
						<div class="col-sm-8">
							<form:select path="roleName" items="${roleList}"
								class="form-select userRole" />
							<form:errors path="roleName" cssClass="error" element="div"
								cssStyle="color : red ;" />
						</div>
					</div>
					<br>
					<div class="row form-group">
						<div class="col-sm-4 label1">
							<label>Email</label>
						</div>
						<div class="col-sm-8">
							<form:input type="text" class="form-control" path="email"
								placeholder="Email" />
							<form:errors path="email" cssClass="error" element="div"
								cssStyle="color : red ;" />
						</div>
					</div>
					<br>
					<div class="row form-group">
						<div class="col-sm-4 label1">
							<label>Address</label>
						</div>
						<div class="col-sm-8">
							<form:input type="text" class="form-control" path="address"
								placeholder="Address" />
							<form:errors path="address" cssClass="error" element="div"
								cssStyle="color : red ;" />
						</div>
					</div>
					<br>
					<div class="row form-group">
						<div class="col-sm-4 label1">
							<label>Phone No</label>
						</div>
						<div class="col-sm-8">
							<form:input type="text" class="form-control" path="phoneNo"
								placeholder="Phone No" />
							<form:errors path="phoneNo" cssClass="error" element="div"
								cssStyle="color : red ;" />
						</div>
					</div>
					<br>
					<div class="none">
						<div class="row form-group">
							<div class="col-sm-4 label1">
								<label>Primary City</label>
							</div>
							<div class="col-sm-8">
								<form:select path="primaryCity" class="form-select primaryCity">
								        <option selected label="--- Select ---" />
									<c:forEach items="${ cityList }" var="city">
										<form:option value="${city.id}" label="${ city.name }" />
									</c:forEach>
								</form:select>
								<form:errors path="primaryCity" cssClass="error" element="div"
									cssStyle="color : red ;" />
							</div>
						</div>
						<br>
						<div class="row form-group">
							<div class="col-sm-4 label1">
								<label>Primary Branch</label>
							</div>
							<div class="col-sm-8">
								<form:select path="primaryBranch" class="form-select primaryBranch">
								        <option selected label="--- Select ---" />
									<c:forEach items="${ branchList }" var="branch">
										<form:option value="${branch.id}" label="${ branch.name }" />
									</c:forEach>
								</form:select>
								<form:errors path="primaryBranch" cssClass="error" element="div"
									cssStyle="color : red ;" />
							</div>
						</div>
						<br>
						<div class="row form-group">
							<div class="col-sm-4 label1">
								<label>Secondary City</label>
							</div>
							<div class="col-sm-6">
								<form:select path="chooseCityId" class="form-select">
									<option selected label="--- Select ---" />
									<c:forEach items="${ cityList }" var="city">
										<form:option value="${city.id}" label="${ city.name }" />
									</c:forEach>
								</form:select>
								<form:errors path="chooseCityId" cssClass="error" element="div"
									cssStyle="color : red ;" />
								<div class="row form-group">
										<ul class="list-group list-group-flush">
											<c:forEach items="${userDTO.secondaryCityList}" var="secondaryCity"
												varStatus="loop">
												<li
													class="list-group-item d-flex justify-content-between align-items-start">
													<div class="ms-2 me-auto">
													<input type="hidden" name="secondaryCityList['${secondaryCity.key}']" value="${secondaryCity.value}"/>
														<div class="fw-bold">${secondaryCity.value}</div>
													</div> <span class="badge btn-outline-danger rounded-pill">
														<button type="button" class="btn-close removeCity" aria-label="Close"
															></button>
												</span>
												</li>
											</c:forEach>
										</ul>
								</div>
							</div>
							<div class="col-sm-2">
								<button class="btn btn-primary" name="add">Add</button>
							</div>
						</div>
						<br>
						<div class="row form-group">
							<div class="col-sm-4 label1">
								<label>Secondary Branch</label>
							</div>
							<div class="col-sm-6">
								<form:select path="chooseBranchId" class="form-select">
									<option selected label="--- Select ---" />
									<c:forEach items="${ branchList }" var="branch">
										<form:option value="${branch.id}" label="${ branch.name }" />
									</c:forEach>
								</form:select>
								<form:errors path="chooseBranchId" cssClass="error" element="div"
									cssStyle="color : red ;" />
								<div class="row form-group">
										<ul class="list-group list-group-flush">
											<c:forEach items="${userDTO.secondaryBranchList}" var="secondaryBranch"
												varStatus="loop">
												<li
													class="list-group-item d-flex justify-content-between align-items-start">
													<div class="ms-2 me-auto">
														<div class="fw-bold">${secondaryBranch.value}</div>
														<input type="hidden" name="secondaryBranchList['${secondaryBranch.key}']" value="${secondaryBranch.value}"/>
													</div> <span class="badge btn-outline-danger rounded-pill">
														<button type="button" class="btn-close removeCity" aria-label="Close"
															></button>
												</span>
												</li>
											</c:forEach>
										</ul>
								</div>
							</div>
							<div class="col-sm-2">
								<button class="btn btn-primary" name="add">Add</button>
							</div>
						</div>
						<br>
					</div>
					<div>
						<form:radiobuttons items="${statusList}" path="status" />
					</div>
					<br>
					<div class=" d-grid gap-2">
						<form:button type="submit" class="btn btn-primary" value="insert">Save</form:button>

						<form:button type="reset" class="btn btn-danger">Clear</form:button>
					</div>
				</div>
			</form:form>
		</div>
		<br>
	</div>
	<div class="col-md-3"></div>
</div>
<br>
<br>
<!-- end for displaying data content -->
<script>
$( document ).ready(function() {
	/* init */
	if($(".userRole").val() != 'ADMIN'){
	$(".none").hide();
	}
	/* end init  */
	/* event method */
	$( ".removeCity" ).on( "click", function() {
		  $(this).parent().parent().remove();
	});
	$(".userRole").on('change', function() {
		  if(this.value == 'ADMIN')$(".none").show();
		  else $(".none").hide();
	});
	$(".primaryCity").on('change', function() {
		 applyPrimaryBranch();
	});
	function applyPrimaryBranch(){
		 var primaryCityId = $(".primaryCity").val();
		  $(".primaryBranch").empty();
		  $
			.ajax({
				type : "GET",
				url : "${pageContext.request.contextPath}/user/getbranchbycity",
				traditional: true,
               dataType:"json",
				
				contentType: 'application/json; charset=utf-8',
				data : {
					'cityId' : primaryCityId
				},
			
				success : function(data) {
					// Parse the returned json data
               console.log(data);
					$.each(data, function(i, item) {
						$('.primaryBranch').append(
								$('<option>', {
									value : item.id,
									text : item.name
								}));

					});
				},
				 error: function (xhr, ajaxOptions, thrownError) {
				        alert(xhr.status);
				        alert(thrownError);
				        console.log(query);
				      }
			});
	}
	/* end event method  */
});
</script>