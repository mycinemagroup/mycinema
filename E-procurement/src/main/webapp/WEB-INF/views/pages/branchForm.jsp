<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/photoframe/style.css" />
<script
	src="${pageContext.request.contextPath}/static/photoframe/script.js"></script>


<!--for displaying data content-->
<div class="row">

	<div class="col-md-3"></div>
	<div class="col-md-6 login-div">
		<div class="row login-div-name">
			<div class="col-xs-8">${ pageTitle }</div>
			<div class="col-xs-4 text-right"><a href="index.do?page=1"><button class="btn btn-primary"><i class="fas fa-arrow-left"></i> Back</button></a></div>
		</div>
		<br>
		<div class="row login-div-content card pb-3" style="background-color:#fff">
		  <form:form modelAttribute="branchDTO" method="POST">
			<div class="col-sm-12">
			<form:hidden path="id"/>
			<form:hidden path="photoUrl" class="photo" />
			   <div class="row row-images">
					<label for="image">Choose Image</label>
					<div class="column image_container">
						<div class="post-image-collection">
						<div class="post-image post-image-placeholder mrm mts empty">
								<div class="upload-section">
									<input type="file" id="Photofile1" class="upload-img"
										accept="image/*" /> <label class="icon-camera"
										for="Photofile1"> <img
										src="${pageContext.request.contextPath}/static/images/camera.png" />
									</label>
									<p class="uppercase">Photo 1</p>
								</div>
								<div class="preview-section">
									<c:if test="${not empty branchDTO.photoUrl}">
										<script> $('.upload-section').hide();ischange=false;</script>
										<img class="thumb mrm mts" src="${branchDTO.photoUrl}"
											title="" />
										<span class="remove_img_preview "></span>
									</c:if>
								</div>
							</div>
						</div>
					</div>
					<button type="button" onclick="upload()" class="btn btn-primary">Upload</button>
				</div>
				<c:if test = "${not empty errorMessage}">
			     <br>
			     <div class="alert alert-danger" role="alert">
                  <i class="fa fa-exclamation-circle" aria-hidden="true"></i> ${errorMessage}
                 </div>
                 </c:if>
						
				<br>
				<div class="row form-group">
					<div class="col-sm-4 label2"><label>Branch Name</label><label class="label-red">*</label></div>
					<div class="col-sm-8">
							<form:input type="text" class="form-control" path="name" placeholder="Branch Name" />
							<form:errors path="name" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>Address</label></div>
					<div class="col-sm-8"><form:textarea class="form-control" path="address" rows="2" placeholder="Address" />
					<form:errors path="address" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>Description</label></div>
					<div class="col-sm-8"><form:textarea class="form-control" path="description" rows="2" placeholder="Description" />
					<form:errors path="description" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>B2B Phone</label></div>
					<div class="col-sm-8"><form:input type="text" class="form-control" path="b2bphone" placeholder="B2B Phone" />
					<form:errors path="b2bphone" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>HotLine Phone1</label></div>
					<div class="col-sm-8"><form:input type="text" class="form-control" path="hotlinePhone1" placeholder="HotLine Phone1" />
					<form:errors path="hotlinePhone1" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>HotLine Phone2</label></div>
					<div class="col-sm-8"><form:input type="text" class="form-control" path="hotlinePhone2" placeholder="HotLine Phone2" />
					<form:errors path="hotlinePhone2" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>HotLine Phone3</label></div>
					<div class="col-sm-8"><form:input type="text" class="form-control" path="hotlinePhone3" placeholder="HotLine Phone3" />
					<form:errors path="hotlinePhone3" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>City</label></div>
					<div class="col-sm-8"><form:select path="cityId" class="form-select">
					<c:forEach items="${ cityList }" var="city">
                                          <form:option value="${city.id}" label="${ city.name }" />
                    </c:forEach>
                                         </form:select>
					<form:errors path="cityId" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div>
                <form:radiobuttons items="${statusList}" path="status"  />
                </div><br>
				<div class=" d-grid gap-2">
						<form:button type="submit" class="btn btn-primary">Save</form:button>
					
						<button type="reset" class="btn btn-danger">Clear</button>
				</div>
			</div>
		  </form:form>
		</div>
		<br>
	</div>
	<div class="col-md-3"></div>
</div>
<br>
<br>
<script>
const firebaseConfig = {
		  apiKey: "AIzaSyB9_EKqAr1-qGsNfjHFduwDfluzh_2hNws",
		  authDomain: "cinemaplatform-e5433.firebaseapp.com",
		  projectId: "cinemaplatform-e5433",
		  storageBucket: "cinemaplatform-e5433.appspot.com",
		  messagingSenderId: "265653901553",
		  appId: "1:265653901553:web:ee256b5e81c639d1577150",
		  measurementId: "G-43Z0J1Y47Y"
		};
		
const appInitialize = firebase.initializeApp(firebaseConfig);	
//Get a reference to the storage service, which is used to create references in your storage bucket
var storage = firebase.storage();
//Create a storage reference from our storage service
var storageRef = storage.ref();
//Points to 'images'
firebase.auth().signInAnonymously()
.then(() => {
  // Signed in..
  console.log("success sign in");
})
.catch((error) => {
  var errorCode = error.code;
  var errorMessage = error.message;
  // ...
  console.log(error);
});

function upload(){
if(typeof(selectedFile) !== "undefined" && selectedFile != null){
	var imagesRef = storageRef.child('images/branch/'+new Date()+'/'+selectedFile.name);
//'file' comes from the Blob or File API
var uploadTask = imagesRef.put(selectedFile);
uploadTask.on('state_changed', 
	    (snapshot) => {
	      // Observe state change events such as progress, pause, and resume
	      // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
	      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
	      console.log('Upload is ' + progress + '% done');
	      switch (snapshot.state) {
	        case firebase.storage.TaskState.PAUSED: // or 'paused'
	          console.log('Upload is paused');
	          break;
	        case firebase.storage.TaskState.RUNNING: // or 'running'
	          console.log('Upload is running');
	          break;
	      }
	    }, 
	    (error) => {
	      // Handle unsuccessful uploads
	    }, 
	    () => {
	      // Handle successful uploads on complete
	      // For instance, get the download URL: https://firebasestorage.googleapis.com/...
	      uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
	    	document.getElementsByClassName("photo")[0].value = downloadURL;  
	        console.log('File available at', downloadURL);
	      });
	    }
	  );
}
}
</script>
<!-- end for displaying data content -->