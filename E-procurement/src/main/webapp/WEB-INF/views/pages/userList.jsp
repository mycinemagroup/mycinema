<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="row div-cover">
	<div class="col-md-12">
		<div class="row content-name">
			<div class="col-xs-8">${pageTitle}</div>
			<div class="col-xs-4 text-right pr-2">
				<a
					href="${pageContext.request.contextPath}/user/create"><button
						class="btn btn-light">
						<i class="fas fa-plus"></i> New
					</button></a>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<form:form modelAttribute="userInquiryDTO" method="POST">
					<div class="row form-group">
						<div class="col-md-2 textbox-pointer-word">
							<form:input type="text" class="form-control" path="username"
								placeholder="Search By Name" />
							<br>
						</div>
						<div class="col-md-2 textbox-pointer-word">
							<form:select path="status" items="${statusList}" class="form-select" />
							<br>
						</div>
						<div class="col-md-2 textbox-pointer-word">
							<form:select path="roleName" items="${roleList}" class="form-select" />
							<br>
						</div>
						<div class="col-md-2">
							<form:button type="submit" class="btn btn-primary btn-block">Search</form:button>
						</div>
					</div>
				</form:form>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12 table-responsive card">
				<table class="table text-wrap table-bordered">
					<thead>
						<tr>
							<th>No</th>
							<th>UserName</th>
							<th>Role</th>
							<th>Email</th>
							<th>Address</th>
							<th>Phone No</th>
							<th>Primary City</th>
							<th>Primary Branch</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${userList}" var="user"
							varStatus="loop">
							<tr>
								<td>${((page-1)*numRow)+(loop.index+1)}</td>
								<td>${user.username }</td>
								<td>${user.roleName }</td>
								<td>${user.email}</td>
								<td>${user.address }</td>
								<td>${user.phoneNo}</td>
								<td>${user.primaryCityName}</td>
								<td>${user.primaryBranchName}</td>
								<td><a href="update?id=${user.id}"><i class="far fa-edit"></i> edit</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>