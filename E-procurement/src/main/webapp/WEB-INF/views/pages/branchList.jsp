<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="row div-cover">
	<div class="col-md-12">
		<div class="row content-name">
			<div class="col-xs-8">${pageTitle}</div>
			<div class="col-xs-4 text-right pr-2">
				<a
					href="${pageContext.request.contextPath}/branch/create"><button
						class="btn btn-light">
						<i class="fas fa-plus"></i> New
					</button></a>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<form:form modelAttribute="branchInquiryDTO" method="POST">
					<div class="row form-group">
						<div class="col-md-2 textbox-pointer-word">
							<form:input type="text" class="form-control" path="name"
								placeholder="Search By Name" />
							<br>
						</div>
						<div class="col-md-2">
							<form:button type="submit" class="btn btn-primary btn-block">Search</form:button>
						</div>
					</div>
				</form:form>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12 table-responsive">
				<table class="table text-wrap table-bordered">
					<thead>
						<tr>
							<th>No</th>
							<th>Name</th>
							<th>Address</th>
							<th>Description</th>
							<th>B2B Phone</th>
							<th>HotLine Phone1</th>
							<th>HotLine Phone2</th>
							<th>HotLine Phone3</th>
							<th>City</th>
							<th>status</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${branchList}" var="branch"
							varStatus="loop">
							<tr>
								<td>${((page-1)*numRow)+(loop.index+1)}</td>
								<td>${branch.name }</td>
								<td>${branch.address }</td>
								<td>${branch.description}</td>
								<td>${branch.b2bphone }</td>
								<td>${branch.hotlinePhone1}</td>
								<td>${branch.hotlinePhone2}</td>
								<td>${branch.hotlinePhone3}</td>
								<td>${branch.city.name}</td>
								<td>${branch.status}</td>
								<td><a href="update?id=${branch.id}"><i class="far fa-edit"></i> edit</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>