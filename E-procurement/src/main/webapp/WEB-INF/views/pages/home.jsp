<div class="callout callout-info">
          <h5>Bootstrap grids</h5>
          <p>Since TheAdmin is based on grid system of Bootstrap, it is necessary to read official documentation of <a href="https://getbootstrap.com/docs/4.0/layout/grid/" target="_blank">grid system</a>.</p>
        </div>