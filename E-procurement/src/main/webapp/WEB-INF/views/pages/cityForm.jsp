<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 

<!--for displaying data content-->
<div class="row">

	<div class="col-md-3"></div>
	<div class="col-md-6 login-div">
		<div class="row login-div-name">
			<div class="col-xs-8">${ pageTitle }</div>
			<div class="col-xs-4 text-right"><a href="index.do?page=1"><button class="btn btn-primary"><i class="fas fa-arrow-left"></i> Back</button></a></div>
		</div>
		<br>
		<div class="row login-div-content card pb-3" style="background-color:#fff">
		  <form:form modelAttribute="cityDTO" method="POST">
			<div class="col-sm-12">
			<form:hidden path="id"/>
				<br>
				<div class="row form-group">
					<div class="col-sm-4 label2"><label>City Name</label><label class="label-red">*</label></div>
					<div class="col-sm-8">
							<form:input type="text" class="form-control" path="name" placeholder="Cinema Name" />
							<form:errors path="name" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div class="row form-group">
					<div class="col-sm-4 label1"><label>Description</label></div>
					<div class="col-sm-8"><form:textarea class="form-control" path="description" rows="5" placeholder="Description" />
					<form:errors path="description" cssClass="error" element="div"
									cssStyle="color : red ;" /></div>
				</div><br>
				<div>
                <form:radiobuttons items="${statusList}" path="status"  />
                </div><br>
				<div class=" d-grid gap-2">
						<form:button type="submit" class="btn btn-primary" value="insert">Save</form:button>
					
						<button type="reset" class="btn btn-danger">Clear</button>
				</div>
			</div>
		  </form:form>
		</div>
		<br>
	</div>
	<div class="col-md-3"></div>
</div>
<br>
<br>
<!-- end for displaying data content -->