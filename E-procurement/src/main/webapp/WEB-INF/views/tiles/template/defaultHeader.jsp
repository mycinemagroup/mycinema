<!-- Topbar -->
    <header class="topbar topbar-inverse" style="background-color: rgb(250,166,75)">
      <div class="topbar-left">
        <span class="topbar-btn sidebar-toggler"></span>

        <div class="topbar-btn d-none d-md-block" data-provide="fullscreen" title="" data-original-title="Fullscreen">
   
        </div>
        <div class="topbar-btn d-none d-md-block" data-provide="fullscreen" title="" data-original-title="Fullscreen">
          <span class="material-icons ">Marvious Cinema</span>
        </div>
        
      </div>

      <div class="topbar-right">
       <ul class="topbar-btns">
          <li class="dropdown">
            <span class="topbar-btn" data-toggle="dropdown"><img class="avatar" src="${pageContext.request.contextPath}/static/images/user.png" alt="..."></span>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="#"><i class="far fa-id-card"></i> Profile</a>
              <a class="dropdown-item" href="#"><i class="fas fa-cog"></i> Settings</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="${pageContext.request.contextPath}/logout"><i class="fas fa-power-off"></i> Logout</a>
            </div>
          </li>
        </ul>

        <div class="topbar-divider"></div>

        <a class="btn btn-xs btn-round btn-primary mr-2" href="#">Dashboard</a>

      </div>
    </header>
    <!-- END Topbar -->