<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!-- Sidebar -->
    <aside class="sidebar sidebar-expand-lg">
      <header class="sidebar-header" style="background-color: rgb(250, 166, 75);">
        <a class="logo-icon" href="#"><span class="icon fa fa-home"></span></a>
        <span class="logo">
          
        </span>
      </header>

      <nav class="sidebar-navigation ps-container ps-theme-default ps-active-x ps-active-y">
        <ul class="menu">

          <li class="menu-category">Preview</li>

          <li class="menu-item">
            <a class="menu-link" href="${pageContext.request.contextPath}/cinema/index?page=1">
              <span class="icon fa fa-list"></span>
              <span class="title">Cinema</span>
            </a>
          </li>
          
          <li class="menu-item">
            <a class="menu-link" href="${pageContext.request.contextPath}/city/index?page=1">
              <span class="icon fas fa-city"></span>
              <span class="title">City</span>
            </a>
          </li>
          
          <li class="menu-item">
            <a class="menu-link" href="${pageContext.request.contextPath}/branch/index?page=1">
              <span class="icon fas fa-city"></span>
              <span class="title">Branch</span>
            </a>
          </li>
          
          <li class="menu-item">
            <a class="menu-link" href="${pageContext.request.contextPath}/user/index?page=1">
              <span class="icon fas fa-city"></span>
              <span class="title">User</span>
            </a>
          </li>

          <li class="menu-item">
            <a class="menu-link" href="#">
              <span class="icon fa fa-tv"></span>
              <span class="title">Samples</span>
              <span class="icon fas fa-angle-down"></span>
            </a>

            <ul class="menu-submenu">
              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Invoicer</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Job Management</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Support System</span>
                </a>
              </li>
            </ul>
          </li>

          <li class="menu-category">Framework</li>


          <li class="menu-item open">
            <a class="menu-link" href="#">
              <span class="icon fa fa-user"></span>
              <span class="title">Layout</span>
              <span class="icon fas fa-angle-down"></span>
            </a>

            <ul class="menu-submenu">
              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Grid</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Layout</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Sidebar</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Topbar</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Header</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Aside</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Card</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Quick view</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Footer</span>
                </a>
              </li>
            </ul>
          </li>


          <li class="menu-item">
            <a class="menu-link" href="#">
              <span class="icon fa fa-align-left"></span>
              <span class="title">Content</span>
              <span class="icon fas fa-angle-down"></span>
            </a>

            <ul class="menu-submenu">
              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Typography</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Colors</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Icons</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Images</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Tables</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Media</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Code</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="#">
                  <span class="dot"></span>
                  <span class="title">Utilities</span>
                </a>
              </li>
            </ul>
          </li>

        </ul>
      <div class="ps-scrollbar-x-rail" style="width: 80px; left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 39px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 560px; right: 2px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 234px;"></div></div></nav>

    </aside>
    <!-- END Sidebar -->