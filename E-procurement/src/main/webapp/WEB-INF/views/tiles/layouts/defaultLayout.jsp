<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<!DOCTYPE html>
<!-- saved from url=(0057)https://thetheme.io/theadmin/layout/sidebar-folded-1.html -->
<html lang="en" style="overflow: -moz-scrollbars-vertical; 
    overflow-y: scroll;">
    <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Responsive admin dashboard and web application ui kit. Sidebar is the main navigation for most of admin templates and web apps.">
    <meta name="keywords" content="sidebar, folded">

    <title>cinema admin</title>

    <!-- Fonts -->
    <link href="${pageContext.request.contextPath}/static/css/all.css" rel="stylesheet">
   
    <!-- Styles -->
    <link href="${pageContext.request.contextPath}/static/css/core.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/app.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/style.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.css" rel="stylesheet">
    
    <!-- jquery -->
    <script src="${pageContext.request.contextPath}/static/jquery/jquery-3.6.0.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/jquery/jquery.datetimepicker.full.min.js"></script>
    <link href="${pageContext.request.contextPath}/static/jquery/jquery.datetimepicker.css" rel="stylesheet">

    <script defer src="${pageContext.request.contextPath}/static/js/all.js"></script>
    <script defer src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/theadmin/core.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/theadmin/app.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/theadmin/script.min.js"></script>
   
   <script src="https://www.gstatic.com/firebasejs/8.9.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.9.0/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.9.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.9.0/firebase-storage.js"></script>
   </head>

  <body class="sidebar-folded  pace-done"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="width: 100%;">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>

    <section id="sidemenu">
            <tiles:insertAttribute name="menu" />
    </section>
    <header id="header">
            <tiles:insertAttribute name="header" />
    </header>
    <!-- Main container -->
    <main class="main-container">

      <div class="main-content" style="padding:80px 20px 0 120px;background-color:#f7f7f7">
       <section id="site-content">
            <tiles:insertAttribute name="body" />
        </section>
      </div><!--/.main-content -->
      
      <footer id="footer">
            <tiles:insertAttribute name="footer" />
        </footer>
    </main>
    <!-- END Main container -->
   
</body></html>