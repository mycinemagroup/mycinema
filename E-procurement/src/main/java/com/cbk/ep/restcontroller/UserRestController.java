package com.cbk.ep.restcontroller;

import java.text.ParseException;

import javax.validation.Valid;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cbk.ep.common.MessageConst;
import com.cbk.ep.dto.MessageDTO;
import com.cbk.ep.dto.UserDTO;
import com.cbk.ep.dto.UserPageDTO;
import com.cbk.ep.entity.JwtResponse;
import com.cbk.ep.entity.User;
import com.cbk.ep.enums.Status;
import com.cbk.ep.exception.CustomWebServiceException;
import com.cbk.ep.service.UserService;
import com.cbk.ep.util.CommonUtil;
import com.cbk.ep.util.JwtTokenUtil;

/**
 * 
 * @author User
 *
 */
@RestController
@CrossOrigin
public class UserRestController {

	private final Logger logger = LoggerFactory.getLogger(UserRestController.class);

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "api/login", method = RequestMethod.POST)
	public ResponseEntity<?> login(@RequestBody UserDTO userDTO) throws JSONException, CustomWebServiceException {
		
		String username = userDTO.getUsername();
		String password = userDTO.getPassword();

		User user = userService.findByUsername(username);
		// not register
		if (user == null) {
			logger.info(MessageConst.INVALID_USERNAME);
			MessageDTO message = new MessageDTO();
			message.setError(true);
			message.setMessage(MessageConst.INVALID_USERNAME);
			return new ResponseEntity<MessageDTO>(message, HttpStatus.UNAUTHORIZED);
		}

		// incorrect password
		if (!passwordEncoder.matches(password, user.getPassword())) {
			logger.info(MessageConst.INCORRECT_PASSWORD);
			MessageDTO message = new MessageDTO();
			message.setError(true);
			message.setMessage(MessageConst.INCORRECT_PASSWORD);
			return new ResponseEntity<MessageDTO>(message, HttpStatus.UNAUTHORIZED);
		}
		// inactive user
		if (user.getStatus().equals(Status.INACTIVE)) {
			logger.info(MessageConst.INACTIVE_USER);
			MessageDTO message = new MessageDTO();
			message.setError(true);
			message.setMessage(MessageConst.INACTIVE_USER);
			return new ResponseEntity<MessageDTO>(message, HttpStatus.UNAUTHORIZED);
		}

		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(username, password));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		userDTO = new UserDTO(user);
		String jwt = jwtTokenUtil.generateToken(userDTO);

		JwtResponse jwtResponse = new JwtResponse(jwt, userDTO);
		return new ResponseEntity<>(jwtResponse, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@RequestMapping(value = "/api/auth/user", method = RequestMethod.POST)
	public ResponseEntity<?> register(@Valid @RequestBody UserDTO user, Errors errors)
			throws CustomWebServiceException, ParseException {
		logger.debug("Start Register User.....");
		logger.debug("Current User :" + CommonUtil.getUsernameFromAuthentication());

		User userExists = userService.findByUsername(user.getUsername());
		if (userExists != null) {
			errors.rejectValue("username", "error.name",
					"*There is already a user registered with the user name provided.");
		}
		// If error, just return a 400 bad request, along with the error message
		if (errors.hasErrors()) {
			logger.error(errors.toString());
			return CommonUtil.getFieldErrors(errors);
		}
		userService.register(user);
		logger.debug("The User name" + user.getUsername() + "end Register......");

		return new ResponseEntity<>(CommonUtil.responseSuccessMessage("Register User Success."), HttpStatus.CREATED);
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@RequestMapping(value = "/api/auth/users", method = RequestMethod.GET)
	public ResponseEntity<?> getAllUser(@Param("name") String name, @Param("role") String role,
			@Param("status") Status status, @PageableDefault(size = 10, sort = "updatedDate") Pageable pageable) {
		logger.debug("Start get all user.....");
		logger.debug("Current Username:" + CommonUtil.getUsernameFromAuthentication());
		UserPageDTO userPage = userService.getAllUser(name, role, status, pageable);
		logger.debug("End get all user.....");
		return new ResponseEntity<>(userPage, HttpStatus.OK);
	}

	@RequestMapping(value = "api/auth/change-password-by-current-user", method = RequestMethod.PUT)
	public ResponseEntity<?> changePasswordByCurrentUser(@RequestBody String data)
			throws CustomWebServiceException, JSONException {

		JSONObject obj = new JSONObject(data);
		String oldPassword = obj.getString("oldPassword");
		String newPassword = obj.getString("newPassword");
		String username = CommonUtil.getUsernameFromAuthentication();
		User user = userService.findByUsername(username);

		if (oldPassword.isEmpty()) {
			throw new CustomWebServiceException("*Please provide your confirm password.");
		}

		if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
			logger.info(MessageConst.INCORRECT_PASSWORD);
			throw new CustomWebServiceException(MessageConst.INCORRECT_PASSWORD);
		}
		userService.changePassword(user.getId(), newPassword);
		MessageDTO message = new MessageDTO();
		message.setMessage("Change password success.");
		return new ResponseEntity<>(message, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@RequestMapping(value = "api/auth/change-password-by-id", method = RequestMethod.PUT)
	public ResponseEntity<?> changePasswordById(@RequestBody String data)
			throws CustomWebServiceException, JSONException {

		logger.debug("Start change password.....");
		logger.debug("Current User :" + CommonUtil.getUsernameFromAuthentication());

		JSONObject obj = new JSONObject(data);
		Long id = obj.getLong("id");
		String newPassword = obj.getString("newPassword");

		userService.changePassword(id, newPassword);
		MessageDTO message = new MessageDTO();
		message.setMessage("Change password success.");

		logger.debug("End change password.....");
		return new ResponseEntity<>(message, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@RequestMapping(value = "api/auth/updateuser", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@RequestBody UserDTO user, Errors errors) throws CustomWebServiceException {

		logger.debug("Start update user.....");
		logger.debug("The user is:" + user);
		logger.debug("Current User :" + CommonUtil.getUsernameFromAuthentication());

		User userExists = userService.findByUsername(user.getUsername());
		if (userExists != null) {
			boolean isCurrentUser = userExists.getId().equals(user.getId());
			if (!isCurrentUser) {
				errors.rejectValue("username", "error.name",
						"*There is already a user registered with the user name provided.");
			}
		}
		// If error, just return a 400 bad request, along with the error message
		if (errors.hasErrors()) {
			logger.error(errors.toString());
			return CommonUtil.getFieldErrors(errors);
		}

		userService.update(user, CommonUtil.getUsernameFromAuthentication());
		logger.debug("End update user....." + "User name is:" + user.getUsername());

		return new ResponseEntity<>(CommonUtil.responseString("Update User Success."), HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@RequestMapping(value = "/api/auth/user/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getUserById(@PathVariable(required = true, name = "id") Long id)
			throws CustomWebServiceException {
		logger.debug("Start get user by id.....");
		UserDTO user = userService.getUserById(id);
		logger.debug("End get user by id.....");
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
}