package com.cbk.ep.exception;

public class CustomWebServiceException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public CustomWebServiceException(){
		super();
	}
	
	public CustomWebServiceException(String message){
		super(message);
	}
	
	public CustomWebServiceException(Throwable cause){
		super(cause);
	}
	
	public CustomWebServiceException(String message, Throwable cause){
		super(message, cause);
	}
}
