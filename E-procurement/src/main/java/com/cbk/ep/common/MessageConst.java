package com.cbk.ep.common;

public class MessageConst {
	public static final String INVALID_USERNAME = "Invalid username.";
	public static final String UNAUTHORIZED = "Sorry, You're not authorized to access this resource.";
	public static final String AUTH_KEY_MISSING = "JWT token missing.";
	public static final String INVALID_JWT = "Invalid JWT.";
	public static final String USER_NOT_FOUND = "User not found.";
	public static final String INCORRECT_PASSWORD = "Incorrect password.";
	public static final String INVALID_ID= "Invalid ID.";
	public static final String INACTIVE_USER = "Sorry, You're not allowed to access. Your status is being INACTIVE.";

}
