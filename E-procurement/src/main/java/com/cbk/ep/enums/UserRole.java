package com.cbk.ep.enums;

public enum UserRole {
	SYSADMIN,ADMIN, OPERATOR,AGENT,CUSTOMER
}
