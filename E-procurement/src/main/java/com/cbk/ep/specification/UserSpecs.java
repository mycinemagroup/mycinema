package com.cbk.ep.specification;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import com.cbk.ep.entity.Role;
import com.cbk.ep.entity.Role_;
import com.cbk.ep.entity.User;
import com.cbk.ep.entity.User_;
import com.cbk.ep.enums.Status;
import com.cbk.ep.enums.UserRole;

public class UserSpecs {

	public static Specification<User> getByNameLikeRoleStatus(String name, UserRole role, Status status,
			Long cinemaId) {
		// TODO Auto-generated method stub
		return new Specification<User>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				// TODO Auto-generated method stub

				final Collection<Predicate> predicates = new ArrayList<>();
				if (!StringUtils.isEmpty(name)) {
					final Predicate namePredicate = criteriaBuilder.like(root.get(User_.USERNAME), "%" + name + "%");
					predicates.add(namePredicate);
				}

				if (!StringUtils.isEmpty(role)) {
					Join<User, Role> roleJoin = root.join(User_.role);
					Predicate rolePredicate = criteriaBuilder.equal(roleJoin.get(Role_.ROLE_NAME), role);
					predicates.add(rolePredicate);
				}

				if (!StringUtils.isEmpty(status)) {
					final Predicate statusPredicate = criteriaBuilder.equal(root.get(User_.STATUS), status);
					predicates.add(statusPredicate);
				}

				Predicate cinemaPredicate = criteriaBuilder.equal(root.get(User_.CINEMA_ID), cinemaId);
				predicates.add(cinemaPredicate);
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}

		};
	}

}
