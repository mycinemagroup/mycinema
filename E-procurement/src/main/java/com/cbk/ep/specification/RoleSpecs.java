package com.cbk.ep.specification;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import com.cbk.ep.entity.Role;
import com.cbk.ep.entity.Role_;
import com.cbk.ep.enums.UserRole;

public class RoleSpecs {

	public static Specification<Role> getAllRole(UserRole name) {
		// TODO Auto-generated method stub
		return new Specification<Role>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<Role> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				// TODO Auto-generated method stub
				final Collection<Predicate> predicates = new ArrayList<>();
				if (!StringUtils.isEmpty(name)) {
					final Predicate namePredicate = criteriaBuilder.equal(root.get(Role_.ROLE_NAME), name);
					predicates.add(namePredicate);
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}

		};
	}

}
