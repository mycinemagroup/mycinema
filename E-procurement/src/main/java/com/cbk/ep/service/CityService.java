package com.cbk.ep.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cbk.ep.dto.CityDTO;
import com.cbk.ep.dto.CityInquiryDTO;
import com.cbk.ep.dto.CityPageDTO;
import com.cbk.ep.entity.City;
import com.cbk.ep.exception.CustomWebServiceException;
import com.cbk.ep.repository.CityRepository;
import com.cbk.ep.util.CommonUtil;
import com.cbk.ep.util.UserPrincipal;

@Service
public class CityService {
	@Autowired
	CityRepository cityRepository;

	public CityPageDTO findByFilter(CityInquiryDTO cityInquiryDTO, Pageable pageable) {
		// TODO Auto-generated method stub
		CityPageDTO cityPageDTO = new CityPageDTO();
		Page<City> page = cityRepository.findByName(cityInquiryDTO.getName(), pageable);
		List<City> cityList = page.getContent();
		List<CityDTO> cityDTOList = new ArrayList<>();
		for (City city : cityList) {
			CityDTO cityDTO = new CityDTO(city);
			cityDTOList.add(cityDTO);
		}

		cityPageDTO.setCityList(cityDTOList);
		cityPageDTO.setNumberofElements(page.getNumberOfElements());
		cityPageDTO.setPage(page.getNumber());
		cityPageDTO.setSize(page.getSize());
		cityPageDTO.setTotalElements(page.getTotalElements());
		cityPageDTO.setTotalPages(page.getTotalPages());
		return cityPageDTO;
	}

	@Transactional(rollbackFor = Exception.class)
	public void register(CityDTO cityDTO) throws CustomWebServiceException {
		// TODO Auto-generated method stub
		UserPrincipal userPrincipal = CommonUtil.getUserPrincipalFromAuthentication();
		City city = new City();
		city.setName(cityDTO.getName());
		city.setDescription(cityDTO.getDescription());
		city.setStatus(cityDTO.getStatus());
		city.setCreatedBy(userPrincipal.getId());
		city.setCreationDate(new Date());
		city.setModifiedBy(userPrincipal.getId());
		city.setModifiedDate(new Date());
		cityRepository.save(city);
	}

	public CityDTO findById(Long id) throws CustomWebServiceException {
		// TODO Auto-generated method stub
		City city = checkValidCity(id);
		CityDTO cityDTO = new CityDTO(city);
		return cityDTO;
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(CityDTO cityDTO) throws CustomWebServiceException {
		// TODO Auto-generated method stub
		UserPrincipal userPrincipal = CommonUtil.getUserPrincipalFromAuthentication();
		City city = checkValidCity(cityDTO.getId());
		city.setName(cityDTO.getName());
		city.setDescription(cityDTO.getDescription());
		city.setStatus(cityDTO.getStatus());
		city.setModifiedBy(userPrincipal.getId());
		city.setModifiedDate(new Date());
		cityRepository.save(city);
	}

	public City checkValidCity(Long id) throws CustomWebServiceException {
		Optional<City> cityOpt = cityRepository.findById(id);
		if (cityOpt.isPresent()) {
			return cityOpt.get();
		} else {
			throw new CustomWebServiceException("Invalid City's Id");
		}
	}
	
	public List<CityDTO> getAllCity() {
		List<City> cityList = cityRepository.findAll();
		List<CityDTO> cityDTOList = new ArrayList<>();
		for(City city : cityList) {
			CityDTO cityDTO = new CityDTO(city);
			cityDTOList.add(cityDTO);
		}
		return cityDTOList;
	}
}
