package com.cbk.ep.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.cbk.ep.common.MessageConst;
import com.cbk.ep.dto.UserDTO;
import com.cbk.ep.dto.UserPageDTO;
import com.cbk.ep.entity.Branch;
import com.cbk.ep.entity.City;
import com.cbk.ep.entity.Role;
import com.cbk.ep.entity.User;
import com.cbk.ep.enums.Status;
import com.cbk.ep.enums.UserRole;
import com.cbk.ep.exception.CustomWebServiceException;
import com.cbk.ep.repository.BranchRepository;
import com.cbk.ep.repository.CityRepository;
import com.cbk.ep.repository.RoleRepository;
import com.cbk.ep.repository.UserRepository;
import com.cbk.ep.specification.UserSpecs;
import com.cbk.ep.util.CommonUtil;
import com.cbk.ep.util.UserPrincipal;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	CityRepository cityRepository;

	@Autowired
	BranchRepository branchRepository;

	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		return userRepository.findByUsername(username);
	}

	public User findById(Long id) {
		// TODO Auto-generated method stub
		return userRepository.findById(id).get();
	}

	@Transactional(rollbackFor = Exception.class)
	public void register(@Valid UserDTO user) throws CustomWebServiceException, ParseException {
		// TODO Auto-generated method stub
		UserPrincipal userPrincipal = CommonUtil.getUserPrincipalFromAuthentication();

		User u = new User();
		User userExists = userRepository.findByUsername(user.getUsername());
		if (userExists != null) {
			throw new CustomWebServiceException("*There is already a user registered with the user name provided.");
		}
		u.setUsername(user.getUsername());
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		u.setPassword(encodedPassword);
		Role role = roleRepository.findByRoleName(UserRole.valueOf(user.getRoleName()));
		if (role == null) {
			throw new CustomWebServiceException("Invalid role name.....");
		}
		u.setRole(role);
		u.setStatus(user.getStatus());
		u.setAddress(user.getAddress());
		u.setEmail(user.getEmail());
		u.setPhoneNo(user.getPhoneNo());

		u.setCreatedBy(userPrincipal.getId());
		u.setCreationDate(new Date());
		u.setModifiedBy(userPrincipal.getId());
		u.setModifiedDate(new Date());
		u.setCinemaId(userPrincipal.getCinemaId());

		u.setPrimaryCity(user.getPrimaryCity());
		u.setPrimaryBranch(user.getPrimaryBranch());

		if (!CollectionUtils.isEmpty(user.getSecondaryCityList())) {
			StringBuilder sb = new StringBuilder();
			user.getSecondaryCityList().forEach((k, v) -> sb.append(k + ","));
			u.setSecondaryCity(sb.toString());
		}
		if (!CollectionUtils.isEmpty(user.getSecondaryBranchList())) {
			StringBuilder sb = new StringBuilder();
			user.getSecondaryBranchList().forEach((k, v) -> sb.append(k + ","));
			u.setSecondaryBranch(sb.toString());
		}

		userRepository.save(u);
	}

	public UserPageDTO getAllUser(String name, String role, Status status, Pageable pageable) {
		// TODO Auto-generated method stub
		UserPrincipal userPrincipal = CommonUtil.getUserPrincipalFromAuthentication();
		UserPageDTO userPage = new UserPageDTO();
		UserRole r = null;
		if (!StringUtils.isEmpty(role))
			r = UserRole.valueOf(role);
		Specification<User> userSpec = UserSpecs.getByNameLikeRoleStatus(name, r, status, userPrincipal.getCinemaId());
		Page<User> page = userRepository.findAll(userSpec, pageable);
		List<User> userList = page.getContent();
		List<UserDTO> userDTOList = new ArrayList<>();
		for (User u : userList) {
			UserDTO uDTO = new UserDTO(u);
			userDTOList.add(uDTO);
		}
		userPage.setUserList(userDTOList);
		userPage.setPage(page.getNumber());
		userPage.setNumberofElements(page.getNumberOfElements());
		userPage.setSize(page.getSize());
		userPage.setTotalElements(page.getTotalElements());
		userPage.setTotalPages(page.getTotalPages());
		return userPage;
	}

	@Transactional(rollbackFor = Exception.class)
	public void changePassword(Long id, String newPassword) throws CustomWebServiceException {
		// TODO Auto-generated method stub
		Optional<User> dbUser = userRepository.findById(id);
		if (dbUser.isPresent()) {
			User u = dbUser.get();
			u.setId(id);
			u.setPassword(passwordEncoder.encode(newPassword));
			userRepository.save(u);
		} else {
			throw new CustomWebServiceException(MessageConst.INVALID_ID);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(UserDTO user, String usernameFromAuthentication) throws CustomWebServiceException {
		// TODO Auto-generated method stub
		UserPrincipal userPrincipal = CommonUtil.getUserPrincipalFromAuthentication();
		Optional<User> dbUser = userRepository.findById(user.getId());
		if (dbUser.isPresent()) {
			User u = dbUser.get();
			u.setUsername(user.getUsername());
			u.setStatus(user.getStatus());
			u.setAddress(user.getAddress());
			u.setEmail(user.getEmail());
			u.setPhoneNo(user.getPhoneNo());
			u.setModifiedBy(userPrincipal.getId());
			u.setModifiedDate(new Date());
			userRepository.save(u);
		} else {
			throw new CustomWebServiceException(MessageConst.INVALID_ID);
		}
	}

	public UserDTO getUserById(Long id) throws CustomWebServiceException {
		// TODO Auto-generated method stub
		Optional<User> user = userRepository.findById(id);
		if (!user.isPresent()) {
			throw new CustomWebServiceException(MessageConst.INVALID_ID);
		}
		UserDTO uDTO = new UserDTO(user.get());
		if (!StringUtils.isEmpty(user.get().getSecondaryCity())) {

			String[] temp = user.get().getSecondaryCity().split(",");
			List<Long> secondaryCityList = Arrays.asList(temp).stream().map(s -> Long.parseLong(s))
					.collect(Collectors.toList());

			Optional<List<City>> cityListOpt = cityRepository.findByIdIn(secondaryCityList);
			List<City> cityList = cityListOpt.get();
			Map<Long, String> secondaryCityMap = new HashMap<>();
			for (City city : cityList) {
				secondaryCityMap.put(city.getId(), city.getName());
			}
			uDTO.setSecondaryCityList(secondaryCityMap);
		}

		if (!StringUtils.isEmpty(user.get().getSecondaryBranch())) {
			String[] temp = user.get().getSecondaryBranch().split(",");
			List<Long> secondaryBranchList = Arrays.asList(temp).stream().map(s -> Long.parseLong(s))
					.collect(Collectors.toList());
			Optional<List<Branch>> branchListOpt = branchRepository.findByIdIn(secondaryBranchList);
			List<Branch> branchList = branchListOpt.get();
			Map<Long, String> secondaryBranchMap = new HashMap<>();
			for (Branch b : branchList) {
				secondaryBranchMap.put(b.getId(), b.getName());
			}
			uDTO.setSecondaryBranchList(secondaryBranchMap);
		}
		return uDTO;
	}
}