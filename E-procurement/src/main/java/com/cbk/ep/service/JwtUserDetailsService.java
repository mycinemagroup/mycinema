package com.cbk.ep.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.cbk.ep.util.UserPrincipal;
import com.cbk.ep.common.MessageConst;
import com.cbk.ep.entity.User;
import com.cbk.ep.repository.UserRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	protected final Logger logger = LoggerFactory.getLogger(JwtUserDetailsService.class);
	
	@Autowired
	private UserRepository repo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = repo.findByUsername(username);
		if (user == null) {
			logger.error(MessageConst.USER_NOT_FOUND);
			throw new UsernameNotFoundException(MessageConst.USER_NOT_FOUND);
		}

		return UserPrincipal.create(user);
	}
}