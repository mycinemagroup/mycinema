package com.cbk.ep.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cbk.ep.dto.BranchDTO;
import com.cbk.ep.dto.BranchInquiryDTO;
import com.cbk.ep.dto.BranchPageDTO;
import com.cbk.ep.entity.Branch;
import com.cbk.ep.entity.City;
import com.cbk.ep.exception.CustomWebServiceException;
import com.cbk.ep.repository.BranchRepository;
import com.cbk.ep.util.CommonUtil;
import com.cbk.ep.util.UserPrincipal;

@Service
public class BranchService {
	@Autowired
	BranchRepository branchRepository;
	
	@Autowired
	CityService cityService;

	public BranchPageDTO findByFilter(BranchInquiryDTO branchInquiryDTO, Pageable pageable) {
		// TODO Auto-generated method stub
		BranchPageDTO branchPageDTO = new BranchPageDTO();
		Page<Branch> page = branchRepository.findByName(branchInquiryDTO.getName(), pageable);
		List<Branch> branchList = page.getContent();
		List<BranchDTO> branchDTOList = new ArrayList<>();
		for (Branch branch : branchList) {
			BranchDTO branchDTO = new BranchDTO(branch);
			branchDTOList.add(branchDTO);
		}

		branchPageDTO.setBranchList(branchDTOList);
		branchPageDTO.setNumberofElements(page.getNumberOfElements());
		branchPageDTO.setPage(page.getNumber());
		branchPageDTO.setSize(page.getSize());
		branchPageDTO.setTotalElements(page.getTotalElements());
		branchPageDTO.setTotalPages(page.getTotalPages());
		return branchPageDTO;
	}

	@Transactional(rollbackFor = Exception.class)
	public void register(BranchDTO branchDTO) throws CustomWebServiceException {
		// TODO Auto-generated method stub
		UserPrincipal userPrincipal = CommonUtil.getUserPrincipalFromAuthentication();
		Branch branch = new Branch();
		branch.setName(branchDTO.getName());
		branch.setAddress(branchDTO.getAddress());
		branch.setB2bphone(branchDTO.getB2bphone());
		branch.setDescription(branchDTO.getDescription());
		branch.setHotlinePhone1(branchDTO.getHotlinePhone1());
		branch.setHotlinePhone2(branchDTO.getHotlinePhone2());
		branch.setHotlinePhone3(branchDTO.getHotlinePhone3());
		branch.setPhotoUrl(branchDTO.getPhotoUrl());
		branch.setStatus(branchDTO.getStatus());
		City city = cityService.checkValidCity(branchDTO.getCityId());
		branch.setCity(city);
		branch.setCreatedBy(userPrincipal.getId());
		branch.setCreationDate(new Date());
		branch.setModifiedBy(userPrincipal.getId());
		branch.setModifiedDate(new Date());
		branch.setCinemaId(userPrincipal.getCinemaId());
		branchRepository.save(branch);
	}

	public BranchDTO findById(Long id) throws CustomWebServiceException {
		// TODO Auto-generated method stub
		Branch branch = checkValidBranch(id);
		BranchDTO branchDTO = new BranchDTO(branch);
		return branchDTO;
	}

	@Transactional(rollbackFor = Exception.class)
	public void update(BranchDTO branchDTO) throws CustomWebServiceException {
		// TODO Auto-generated method stub
		UserPrincipal userPrincipal = CommonUtil.getUserPrincipalFromAuthentication();
		Branch branch = checkValidBranch(branchDTO.getId());
		branch.setName(branchDTO.getName());
		branch.setAddress(branchDTO.getAddress());
		branch.setB2bphone(branchDTO.getB2bphone());
		branch.setDescription(branchDTO.getDescription());
		branch.setHotlinePhone1(branchDTO.getHotlinePhone1());
		branch.setHotlinePhone2(branchDTO.getHotlinePhone2());
		branch.setHotlinePhone3(branchDTO.getHotlinePhone3());
		branch.setPhotoUrl(branchDTO.getPhotoUrl());
		branch.setStatus(branchDTO.getStatus());
		City city = cityService.checkValidCity(branchDTO.getCityId());
		branch.setCity(city);
		branch.setCinemaId(userPrincipal.getCinemaId());
		branch.setModifiedBy(userPrincipal.getId());
		branch.setModifiedDate(new Date());
		branchRepository.save(branch);
	}

	public Branch checkValidBranch(Long id) throws CustomWebServiceException {
		Optional<Branch> branchOpt = branchRepository.findById(id);
		if (branchOpt.isPresent()) {
			return branchOpt.get();
		} else {
			throw new CustomWebServiceException("Invalid Branch's Id");
		}
	}

	public List<BranchDTO> getBranchByCity(List<Long> cityIdList) {
		// TODO Auto-generated method stub
		UserPrincipal userPrincipal = CommonUtil.getUserPrincipalFromAuthentication();
		List<Branch> branchList = branchRepository.findByCityIdAndCinemaId(cityIdList, userPrincipal.getCinemaId());
		List<BranchDTO> branchDTOList = new ArrayList<>();
		for(Branch branch : branchList) {
			BranchDTO branchDTO = new BranchDTO(branch);
			branchDTOList.add(branchDTO);
		}
		return branchDTOList;
	}

	public List<BranchDTO> findByIdIn(List<Long> branchIdList) {
		// TODO Auto-generated method stub
		List<Branch> branchList = branchRepository.findbyIdIn(branchIdList);
		List<BranchDTO> branchDTOList = new ArrayList<>();
		for(Branch b : branchList) {
			BranchDTO bdto = new BranchDTO(b);
			branchDTOList.add(bdto);
		}
		return branchDTOList;
	}
}
