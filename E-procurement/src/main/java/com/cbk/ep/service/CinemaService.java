package com.cbk.ep.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cbk.ep.dto.CinemaDTO;
import com.cbk.ep.dto.CinemaInquiryDTO;
import com.cbk.ep.dto.CinemaPageDTO;
import com.cbk.ep.entity.Cinema;
import com.cbk.ep.entity.User;
import com.cbk.ep.enums.Status;
import com.cbk.ep.exception.CustomWebServiceException;
import com.cbk.ep.repository.CinemaRepository;
import com.cbk.ep.repository.UserRepository;
import com.cbk.ep.util.CommonUtil;
import com.cbk.ep.util.UserPrincipal;

@Service
public class CinemaService {

	@Autowired
	CinemaRepository cinemaRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	public CinemaPageDTO findByFilter(CinemaInquiryDTO cinemaInquiryDTO, Pageable pageable) {
		// TODO Auto-generated method stub
		CinemaPageDTO cinemaPageDTO = new CinemaPageDTO();
		Page<Cinema> page = cinemaRepository.findByName(cinemaInquiryDTO.getName(), pageable);
		List<Cinema> cinemaList = page.getContent();
		List<CinemaDTO> cinemaDTOList = new ArrayList<>();
		for (Cinema cinema : cinemaList) {
			CinemaDTO cinemaDTO = new CinemaDTO(cinema);
			cinemaDTOList.add(cinemaDTO);
		}

		cinemaPageDTO.setCinemaList(cinemaDTOList);
		cinemaPageDTO.setNumberofElements(page.getNumberOfElements());
		cinemaPageDTO.setPage(page.getNumber());
		cinemaPageDTO.setSize(page.getSize());
		cinemaPageDTO.setTotalElements(page.getTotalElements());
		cinemaPageDTO.setTotalPages(page.getTotalPages());
		return cinemaPageDTO;
	}

	public CinemaDTO findById(Long id) throws CustomWebServiceException {
		Optional<Cinema> cinemaOpt = cinemaRepository.findById(id);
		if (!cinemaOpt.isPresent()) {
			throw new CustomWebServiceException("Invalid Cinema Id");
		}
		CinemaDTO cinemaDTO = new CinemaDTO(cinemaOpt.get());
		return cinemaDTO;
	}

	@Transactional(rollbackFor = Exception.class)
	public CinemaDTO register(CinemaDTO cinemaDTO) throws CustomWebServiceException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

		Optional<Cinema> cinemaOpt = cinemaRepository.findByName(cinemaDTO.getName());
		if(cinemaOpt.isPresent()) {
			throw new CustomWebServiceException("Cinema with "+cinemaDTO.getName()+ " already exists.");
		}
		Cinema cinema = new Cinema();
		cinema.setName(cinemaDTO.getName());
		cinema.setAddress(cinemaDTO.getAddress());
		cinema.setAdminJson(cinemaDTO.getAdminJson());
		cinema.setDetails(cinemaDTO.getDetails());
		cinema.setDatabaseURL(cinemaDTO.getDatabaseURL());
		cinema.setFirebaseInitializeAppConfig(cinemaDTO.getFirebaseInitializeAppConfig());
		cinema.setPhno(cinemaDTO.getPhno());
		cinema.setPhotoUrl(cinemaDTO.getPhotoUrl());
		cinema.setStatus(cinemaDTO.getStatus());
		cinema.setCreationDate(new Date());
		cinema.setCreatedBy(userPrincipal.getId());
		cinema = cinemaRepository.save(cinema);

		User user = new User();
		user.setUsername(cinemaDTO.getCoreAdminUserName());
		String encodedPassword = passwordEncoder.encode(cinemaDTO.getCoreAdminPassword());
		user.setPassword(encodedPassword);
		user.setCreatedBy(userPrincipal.getId());
		user.setCreationDate(new Date());
		user.setModifiedBy(userPrincipal.getId());
		user.setModifiedDate(new Date());
		user.setCinemaId(cinema.getId());
		user.setStatus(Status.ACTIVE);
		userRepository.save(user);

		return cinemaDTO;
	}

	@Transactional(rollbackFor = Exception.class)
	public CinemaDTO update(CinemaDTO cinemaDTO) throws CustomWebServiceException {
		// TODO Auto-generated method stub
		UserPrincipal userPrincipal = CommonUtil.getUserPrincipalFromAuthentication();

		Cinema cinema = checkValidCinema(cinemaDTO.getId());
		cinema.setName(cinemaDTO.getName());
		cinema.setAddress(cinemaDTO.getAddress());
		cinema.setAdminJson(cinemaDTO.getAdminJson());
		cinema.setDetails(cinemaDTO.getDetails());
		cinema.setDatabaseURL(cinemaDTO.getDatabaseURL());
		cinema.setFirebaseInitializeAppConfig(cinemaDTO.getFirebaseInitializeAppConfig());
		cinema.setPhno(cinemaDTO.getPhno());
		cinema.setPhotoUrl(cinemaDTO.getPhotoUrl());
		cinema.setStatus(cinemaDTO.getStatus());
		cinema.setModifiedDate(new Date());
		cinema.setModifiedBy(userPrincipal.getId());
		cinemaRepository.save(cinema);
		return cinemaDTO;
	}

	public Cinema checkValidCinema(Long id) throws CustomWebServiceException {
		Optional<Cinema> cinemaOpt = cinemaRepository.findById(id);
		if (cinemaOpt.isPresent()) {
			return cinemaOpt.get();
		} else {
			throw new CustomWebServiceException("Invalid Cinema's Id");
		}
	}
}
