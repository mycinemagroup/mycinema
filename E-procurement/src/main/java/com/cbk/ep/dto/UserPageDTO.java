package com.cbk.ep.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class UserPageDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	List<UserDTO> userList = new ArrayList<>();
	int page;
	int size;
	int numberofElements;
	long totalElements;
	int totalPages;

}
