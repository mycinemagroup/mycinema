package com.cbk.ep.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class CityInquiryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1445917209947383289L;
    private String name;
}
