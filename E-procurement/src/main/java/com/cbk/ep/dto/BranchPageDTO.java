package com.cbk.ep.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class BranchPageDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7745543649520898856L;
	List<BranchDTO> branchList = new ArrayList<>();
	int page;
	int size;
	int numberofElements;
	long totalElements;
	int totalPages;
}
