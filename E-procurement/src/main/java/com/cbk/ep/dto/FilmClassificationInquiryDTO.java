package com.cbk.ep.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class FilmClassificationInquiryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7070714438343967370L;
	private String name;
	private String status;
}
