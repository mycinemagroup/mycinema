package com.cbk.ep.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class BranchInquiryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6512285057559060058L;
	private String name;
}
