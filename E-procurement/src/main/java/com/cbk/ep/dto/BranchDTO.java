package com.cbk.ep.dto;

import com.cbk.ep.entity.Branch;
import com.cbk.ep.enums.Status;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class BranchDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4317386838237574917L;
	private Long id;

	private String name;

	private String description;

	private String address;

	private String b2bphone;

	private String hotlinePhone1;

	private String hotlinePhone2;

	private String hotlinePhone3;

	private String photoUrl;

	private CityDTO city;
	
	private Long cityId;

	private Status status = Status.ACTIVE;

	public BranchDTO() {
		super();
	}

	public BranchDTO(Branch entity) {
		this.id = entity.getId();
		this.name = entity.getName();
		this.description = entity.getDescription();
		this.address = entity.getAddress();
		this.b2bphone = entity.getB2bphone();
		this.hotlinePhone1 = entity.getHotlinePhone1();
		this.hotlinePhone2 = entity.getHotlinePhone2();
		this.hotlinePhone3 = entity.getHotlinePhone3();
		this.photoUrl = entity.getPhotoUrl();
		this.city = entity.getCity() == null ? null : new CityDTO(entity.getCity());
		this.cityId = entity.getCity() == null ? null : entity.getCity().getId();
		this.status = entity.getStatus();
		this.setCreatedBy(entity.getCreatedBy());
		this.setCreationDate(entity.getCreationDate());
		this.setModifiedBy(entity.getModifiedBy());
		this.setModifiedDate(entity.getModifiedDate());
		this.setCinemaId(entity.getCinemaId());
	}
}
