package com.cbk.ep.dto;

import com.cbk.ep.entity.FilmClassification;
import com.cbk.ep.enums.Status;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class FilmClassificationDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3476594368627370307L;
	private Long id;
	private String name;
	private String description;
	private Status status = Status.ACTIVE;

	public FilmClassificationDTO() {
		super();
	}

	public FilmClassificationDTO(FilmClassification entity) {
		this.id = entity.getId();
		this.name = entity.getName();
		this.description = entity.getDescription();
		this.status = entity.getStatus();
		this.setCreatedBy(entity.getCreatedBy());
		this.setCreationDate(entity.getCreationDate());
		this.setModifiedBy(entity.getModifiedBy());
		this.setModifiedDate(entity.getModifiedDate());
	}
}
