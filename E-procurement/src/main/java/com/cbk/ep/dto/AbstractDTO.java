package com.cbk.ep.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AbstractDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1623325875525415131L;
	/**
	 * Creation date.
	 */
	private Date creationDate;
	/**
	 * Modified date.
	 */
	private Date modifiedDate;

	/**
	 * created by user id.
	 */
	private Long createdBy;

	/**
	 * modified by user id.
	 */
	private Long modifiedBy;

	private Long specialId;
	private Long cinemaId;

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Long getSpecialId() {
		return specialId;
	}

	public void setSpecialId(Long specialId) {
		this.specialId = specialId;
	}

	public Long getCinemaId() {
		return cinemaId;
	}

	public void setCinemaId(Long cinemaId) {
		this.cinemaId = cinemaId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
