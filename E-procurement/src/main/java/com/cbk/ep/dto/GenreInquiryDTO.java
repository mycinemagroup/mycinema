package com.cbk.ep.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class GenreInquiryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1022438177193127075L;
	private String name;
	private String status;
}
