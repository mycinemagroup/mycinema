package com.cbk.ep.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.cbk.ep.entity.Role;
import com.cbk.ep.enums.UserRole;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class RoleDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	@NotNull(message = "Please provide role name.")
	private UserRole roleName;
	private String description;

	public RoleDTO() {
	}

	public RoleDTO(Role entity) {
		this.id = entity.getId();
		this.roleName = entity.getRoleName();
		this.description = entity.getDescription();
	}

}
