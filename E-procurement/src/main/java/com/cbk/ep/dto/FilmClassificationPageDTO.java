package com.cbk.ep.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class FilmClassificationPageDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1270979871609370236L;
	List<FilmClassificationDTO> filmClassificationList = new ArrayList<>();
	int page;
	int size;
	int numberofElements;
	long totalElements;
	int totalPages;
}
