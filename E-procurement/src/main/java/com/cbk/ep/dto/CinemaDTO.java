package com.cbk.ep.dto;

import java.util.Date;

import com.cbk.ep.entity.Cinema;
import com.cbk.ep.enums.Status;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class CinemaDTO extends AbstractDTO {
	/**
	* 
	*/
	private static final long serialVersionUID = 91089563117692042L;
	private Long id;
	private String name;
	private String address;
	private String phno;
	private String photoUrl;
	private String details;
	private Status status = Status.ACTIVE;
	private String adminJson;
	private String firebaseInitializeAppConfig;
	private String databaseURL;
	private boolean statusBoolean = true;
	private Long createdBy;
	private Date creationDate;
	private Long modifiedBy;
	private Date modifiedDate;
	
	// core admin
	private String coreAdminUserName;
	private String coreAdminPassword;
	
	public CinemaDTO() {
		super();
	}
	public CinemaDTO(Cinema entity) {
		this.id = entity.getId();
		this.name = entity.getName();
		this.address = entity.getAddress();
		this.phno = entity.getPhno();
		this.photoUrl = entity.getPhotoUrl();
		this.details = entity.getDetails();
		this.status = entity.getStatus();
		this.adminJson = entity.getAdminJson();
		this.firebaseInitializeAppConfig = entity.getFirebaseInitializeAppConfig();
		this.databaseURL = entity.getDatabaseURL();
		this.createdBy = entity.getCreatedBy();
		this.creationDate = entity.getCreationDate();
		this.modifiedBy = entity.getModifiedBy();
		this.modifiedDate = entity.getModifiedDate();
	}
}
