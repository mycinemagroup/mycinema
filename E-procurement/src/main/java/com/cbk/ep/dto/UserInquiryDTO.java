package com.cbk.ep.dto;

import java.io.Serializable;

import com.cbk.ep.enums.Status;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class UserInquiryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4928842096885557396L;
    private String username;
    private Status status;
    private String roleName;
}
