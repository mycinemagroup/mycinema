package com.cbk.ep.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class RolePageDTO implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<RoleDTO> roleList = new ArrayList<>();
	int page;
	int size;
	int numberofElements;
	long totalElements;
	int totalPages;
}
