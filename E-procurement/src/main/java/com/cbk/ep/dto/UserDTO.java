package com.cbk.ep.dto;

import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.cbk.ep.entity.User;
import com.cbk.ep.enums.Status;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class UserDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	@NotBlank(message = "Please provide name.")
	@Length(max = 100, message = "Name length must not be greater than 100.")
	private String username;

	@NotBlank(message = "Please provide password.")
	@Length(max = 50, message = "Password length must not be greater than 50.")
	private String password;
	
	private String confirmpassword;

	private String roleName;
	private RoleDTO role;

	private Status status = Status.ACTIVE;

	private String email;

	private String address;

	private String phoneNo;

	private Long primaryCity;

	private Long primaryBranch;
	
	private String primaryCityName;
	
	private String primaryBranchName;
	
	private Map<Long,String> secondaryCityList = new HashMap<>();
	
	private Map<Long,String> secondaryBranchList = new HashMap<>();
	
	private Long chooseCityId;
	
	private Long chooseBranchId;

	public UserDTO() {
		super();
	}

	public UserDTO(User entity) {
		this.id = entity.getId();
		this.username = entity.getUsername();
		this.role = new RoleDTO(entity.getRole());
		this.roleName = entity.getRole().getRoleName().toString();
		this.status = entity.getStatus();
		this.email = entity.getEmail();
		this.address = entity.getAddress();
		this.phoneNo = entity.getPhoneNo();
		this.primaryCity = entity.getPrimaryCity();
		this.primaryBranch = entity.getPrimaryBranch();
		this.setCreatedBy(entity.getCreatedBy());
		this.setCreationDate(entity.getCreationDate());
		this.setModifiedBy(entity.getModifiedBy());
		this.setModifiedDate(entity.getModifiedDate());
		this.setCinemaId(entity.getCinemaId());
	}

}
