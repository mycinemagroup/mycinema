package com.cbk.ep.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cbk.ep.entity.Branch;

@Repository
public interface BranchRepository extends JpaRepository<Branch, Long>, JpaSpecificationExecutor<Branch> {

	@Query(value = "SELECT * FROM branch b "
			+ "WHERE (b.name LIKE CONCAT('%',:name,'%') OR :name IS NULL) "
			+ "ORDER BY b.modified_date desc "
			, nativeQuery = true)
	Page<Branch> findByName(@Param("name") String name, Pageable pageable);

	@Query(value = "SELECT * FROM branch b "
			+ "WHERE b.city_id IN ?1 "
			+ "AND b.cinema_id = ?2 "
			+ "AND b.status = 'ACTIVE' "
			, nativeQuery = true)
	List<Branch> findByCityIdAndCinemaId(List<Long> cityIdList, Long cinemaId);

	@Query(value = "SELECT * FROM branch b "
			+ "WHERE b.id IN ?1 "
			, nativeQuery = true)
	List<Branch> findbyIdIn(List<Long> branchIdList);

	@Query(value = "SELECT * FROM branch b "
			+ "WHERE (b.id IN :secondaryBranch) "
			, nativeQuery = true)
	Optional<List<Branch>> findByIdIn(@Param("secondaryBranch")List<Long> secondaryBranch);

}
