package com.cbk.ep.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cbk.ep.entity.City;

@Repository
public interface CityRepository extends JpaRepository<City, Long>, JpaSpecificationExecutor<City> {

	@Query(value = "SELECT * FROM city c "
			+ "WHERE (c.name LIKE CONCAT('%',:name,'%') OR :name IS NULL) "
			+ "ORDER BY c.modified_date desc "
			, nativeQuery = true)
	Page<City> findByName(@Param("name") String name, Pageable pageable);

	@Query(value = "SELECT * FROM city c "
			+ "WHERE (c.id IN :secondaryCity) "
			, nativeQuery = true)
	Optional<List<City>> findByIdIn(@Param("secondaryCity")List<Long> secondaryCity);

}
