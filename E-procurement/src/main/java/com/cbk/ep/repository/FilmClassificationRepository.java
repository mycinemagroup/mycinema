package com.cbk.ep.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cbk.ep.entity.FilmClassification;

@Repository
public interface FilmClassificationRepository extends JpaRepository<FilmClassification, Long>, JpaSpecificationExecutor<FilmClassification> {

}
