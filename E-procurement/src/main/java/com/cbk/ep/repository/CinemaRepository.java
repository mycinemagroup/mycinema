package com.cbk.ep.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cbk.ep.entity.Cinema;

@Repository
public interface CinemaRepository extends JpaRepository<Cinema, Long>, JpaSpecificationExecutor<Cinema>{

	@Query(value = "Select * from cinema c "
			+ "WHERE (c.name LIKE CONCAT('%',?1,'%') OR ?1 IS NULL) "
			,nativeQuery = true)
	Page<Cinema> findByName(String name, Pageable pageable);

	Optional<Cinema> findByName(String name);

}
