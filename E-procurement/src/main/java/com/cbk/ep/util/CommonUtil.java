package com.cbk.ep.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import com.cbk.ep.dto.MessageDTO;
import com.cbk.ep.exception.CustomWebServiceException;

public class CommonUtil {
	
	public static final Integer numberOfElements20 = 20;

	/**
	 * Change String to JSON Format String
	 * 
	 * @param message
	 * @return
	 */
	public static String responseString(String message) {
		Map<String, String> responseMap = new HashMap<>();
		responseMap.put("message", message);

		return new JSONObject(responseMap).toString();
	}

	/**
	 * Response success message
	 * 
	 * @param message
	 * @return
	 */
	public static MessageDTO responseSuccessMessage(String message) {
		MessageDTO messageDTO = new MessageDTO();
		messageDTO.setMessage(message);
		return messageDTO;
	}

	/**
	 * Get username from authentication object
	 * 
	 * @return
	 */
	public static String getUsernameFromAuthentication() {
		String username = null;
		Authentication auth = (Authentication) SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			Object p = auth.getPrincipal();
			if (p instanceof UserDetails) {
				username = ((UserDetails) p).getUsername();
			}
		}
		return username;
	}
	
	public static UserPrincipal getUserPrincipalFromAuthentication() {
		UserPrincipal userPrincipal = null;
		UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) SecurityContextHolder
				.getContext().getAuthentication();
		if (auth != null) {
			Object p = auth.getPrincipal();
			if (p instanceof UserPrincipal) {
				userPrincipal = (UserPrincipal) p;
			}
		}
		return userPrincipal;
	}

	/**
	 * @param imagePath
	 * @return
	 * @throws CustomWebServiceException
	 */
	public static String encoder(String imagePath) throws CustomWebServiceException {
		String base64Image = "";
		File file = new File(imagePath);
		try (FileInputStream imageInFile = new FileInputStream(file)) {
			// Reading a Image file from file system
			byte imageData[] = new byte[(int) file.length()];
			imageInFile.read(imageData);
			base64Image = Base64.getEncoder().encodeToString(imageData);
		} catch (FileNotFoundException e) {

			throw new CustomWebServiceException("Image not fount...");

		} catch (IOException ioe) {

			throw new CustomWebServiceException("Exception while reading the Image.....");
		}
		return base64Image;
	}

	/**
	 * @param base64Image
	 * @return
	 * @throws CustomWebServiceException
	 */
	public static BufferedImage decoder(String base64Image) throws CustomWebServiceException {
		BufferedImage image = null;
		try {
			// Converting a Base64 String into Image byte array
			byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByteArray);
			image = ImageIO.read(bis);
			bis.close();
		} catch (Exception e) {

			throw new CustomWebServiceException("Image decode exception.....");

		}
		return image;
	}

	/**
	 * @param dateTime
	 * @param standardDateInputFormat
	 * @return
	 */
	public static Date stringToDate(String dateTime, String standardDateInputFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(standardDateInputFormat);
		Date retDate = new Date();
		try {
			if (!dateTime.isEmpty())
				retDate = sdf.parse(dateTime);
		} catch (ParseException e) {
		}
		return retDate;
	}

	/**
	 * @param dateTime
	 * @param standardDateInputFormat
	 * @return
	 */
	public static String dateToString(Date dateTime, String standardDateInputFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(standardDateInputFormat);
		String retDate = new String();
		if (dateTime != null)
			retDate = sdf.format(dateTime);
		return retDate;
	}

	/**
	 * @param errors
	 * @return
	 */
	public static ResponseEntity<?> getFieldErrors(Errors errors) {
		MessageDTO message = new MessageDTO();
		message.setError(true);
		HashMap<String, String> errorMessages = new HashMap<>();
		for (FieldError fieldErr : errors.getFieldErrors()) {
			errorMessages.put(fieldErr.getField(), fieldErr.getDefaultMessage());
		}
		message.setFieldErrorMessages(errorMessages);
		return new ResponseEntity<>(message, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	/**
	 * @param base64EncodedString
	 * @return
	 */
	public static String tokenizeBase64EncodedStringToImageStream(String base64EncodedString) {
		String delimiter = "[,]";
		String[] parts = base64EncodedString.split(delimiter);
		String imageStream = parts[1];
		return imageStream;
	}

	/**
	 * @param base64EncodedString
	 * @return
	 */
	public static String getFielExtensionFromBase64EncodedString(String base64EncodedString) {
		int extentionStartIndex = base64EncodedString.indexOf('/');
		int extensionEndIndex = base64EncodedString.indexOf(';');
		String fileExtension = base64EncodedString.substring(extentionStartIndex + 1, extensionEndIndex);
		return fileExtension;
	}

	/**
	 * @param date
	 * @param standardDateInputFormat
	 * @return
	 * @throws ParseException
	 */
	public static Date getDateWithoutTimeUsingFormat(Date date, String standardDateInputFormat) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(standardDateInputFormat);
		return formatter.parse(formatter.format(date));
	}
}
