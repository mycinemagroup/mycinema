package com.cbk.ep.controller;

import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cbk.ep.dto.BranchDTO;
import com.cbk.ep.dto.BranchInquiryDTO;
import com.cbk.ep.dto.BranchPageDTO;
import com.cbk.ep.dto.CityDTO;
import com.cbk.ep.enums.Status;
import com.cbk.ep.service.BranchService;
import com.cbk.ep.service.CityService;
import com.cbk.ep.util.CommonUtil;

@Controller
@RequestMapping("/branch")
public class BranchController extends AbstractMvcController {

	@Autowired
	BranchService branchService;

	@Autowired
	CityService cityService;

	// get all list
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView getlistData(@RequestParam("page") Integer page,
			@ModelAttribute("branchInquiryDTO") BranchInquiryDTO branchInquiryDTO, HttpSession session,
			Authentication authentication) {

			if (this.containsKey("branchInquiryDTO")) {
				branchInquiryDTO = (BranchInquiryDTO) this.getViewObject("branchInquiryDTO");
			} else
				branchInquiryDTO = new BranchInquiryDTO();
		

		Pageable pageable = PageRequest.of(page - 1, CommonUtil.numberOfElements20);
		try {
			BranchPageDTO branchPageDTO = branchService.findByFilter(branchInquiryDTO, pageable);
			this.addViewObject("branchList", branchPageDTO.getBranchList());
			this.addViewObject("branchInquiryDTO", branchInquiryDTO);
			this.addViewObject("page", page);
			addViewObject("pageTitle", "Branch List");
		} catch (Exception e) {

		}
		return new ModelAndView("branch_List", this.getViewModelMap());
	}

	// search by inquiryDTO
	@RequestMapping(value = "/index", method = RequestMethod.POST)
	public String inquireByFilter(@ModelAttribute BranchInquiryDTO branchInquiryDTO, HttpSession session) {
		this.addViewObject("branchInquiryDTO", branchInquiryDTO);
		// cinemaApplicationService.inquireCinemaByName(sessionContext, name);
		return "redirect:index?page=" + 1;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView createForm(HttpSession session, @ModelAttribute("branchDTO") BranchDTO branchDTO) {
		List<CityDTO> cityList = cityService.getAllCity();
		if(Objects.isNull(branchDTO))
		branchDTO = new BranchDTO();
		addViewObject("branchDTO", branchDTO);
		addViewObject("statusList", Status.values());
		addViewObject("cityList", cityList);
		addViewObject("pageTitle", "Branch");
		return new ModelAndView("branch_Form", getViewModelMap());
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String saveBranch(@ModelAttribute("branchDTO") BranchDTO branchDTO, RedirectAttributes redirectAttributes,
			Authentication auth) {

		try {
			branchService.register(branchDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
			redirectAttributes.addFlashAttribute("branchDTO", branchDTO);
			e.printStackTrace();
			return "redirect:create";
		}
		return "redirect:index?page=" + 1;
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public ModelAndView updateBranch(@RequestParam("id") Long id, HttpSession session) {
		BranchDTO branchDTO = null;
		try {
			branchDTO = branchService.findById(id);
			List<CityDTO> cityList = cityService.getAllCity();
			addViewObject("cityList", cityList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		addViewObject("branchDTO", branchDTO);
		addViewObject("statusList", Status.values());
		addViewObject("pageTitle", "Branch");
		return new ModelAndView("branch_Form", getViewModelMap());
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String saveUpdate(@ModelAttribute("branchDTO") BranchDTO branchDTO, RedirectAttributes redirectAttributes) {

		try {
			branchService.update(branchDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
			e.printStackTrace();
			return "redirect:update?id="+ branchDTO.getId();
		}
		return "redirect:index.do?page=" + 1;
	}
}
