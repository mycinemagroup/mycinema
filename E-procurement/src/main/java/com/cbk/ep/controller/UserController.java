package com.cbk.ep.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cbk.ep.dto.BranchDTO;
import com.cbk.ep.dto.CityDTO;
import com.cbk.ep.dto.UserDTO;
import com.cbk.ep.dto.UserInquiryDTO;
import com.cbk.ep.dto.UserPageDTO;
import com.cbk.ep.enums.Status;
import com.cbk.ep.enums.UserRole;
import com.cbk.ep.exception.CustomWebServiceException;
import com.cbk.ep.service.BranchService;
import com.cbk.ep.service.CityService;
import com.cbk.ep.service.UserService;
import com.cbk.ep.util.CommonUtil;

@Controller
@RequestMapping("/user")
public class UserController extends AbstractMvcController {

	@Autowired
	UserService userService;

	@Autowired
	CityService cityService;

	@Autowired
	BranchService branchService;

	// get all list
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView getlistData(@RequestParam("page") Integer page,
			@ModelAttribute("userInquiryDTO") UserInquiryDTO userInquiryDTO, HttpSession session,
			Authentication authentication) {

		if (this.containsKey("userInquiryDTO")) {
			userInquiryDTO = (UserInquiryDTO) this.getViewObject("userInquiryDTO");
		} else
			userInquiryDTO = new UserInquiryDTO();

		Pageable pageable = PageRequest.of(page - 1, CommonUtil.numberOfElements20);
		try {
			UserPageDTO userPageDTO = userService.getAllUser(userInquiryDTO.getUsername(), userInquiryDTO.getRoleName(),
					userInquiryDTO.getStatus(), pageable);
			this.addViewObject("userList", userPageDTO.getUserList());
			this.addViewObject("roleList", UserRole.values());
			this.addViewObject("userInquiryDTO", userInquiryDTO);
			this.addViewObject("page", page);
			addViewObject("pageTitle", "User List");
		} catch (Exception e) {

		}
		return new ModelAndView("user_List", this.getViewModelMap());
	}

	// search by inquiryDTO
	@RequestMapping(value = "/index", method = RequestMethod.POST)
	public String inquireByFilter(@ModelAttribute("userInquiryDTO") UserInquiryDTO userInquiryDTO,
			HttpSession session) {
		this.addViewObject("userInquiryDTO", userInquiryDTO);
		// cinemaApplicationService.inquireCinemaByName(sessionContext, name);
		return "redirect:index?page=" + 1;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView createForm(HttpSession session, @ModelAttribute("userDTO") UserDTO userDTO) {
		List<CityDTO> cityList = cityService.getAllCity();
		if (Objects.isNull(userDTO))
			userDTO = new UserDTO();
		addViewObject("userDTO", userDTO);
		addViewObject("statusList", Status.values());
		addViewObject("cityList", cityList);
		addViewObject("pageTitle", "User");
		return new ModelAndView("user_Form", getViewModelMap());
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute("userDTO") UserDTO userDTO, RedirectAttributes redirectAttributes,
			Authentication auth) {

		try {
			if (!Objects.isNull(userDTO.getPrimaryBranch())) {
				BranchDTO branchDTO = branchService.findById(userDTO.getPrimaryBranch());
				if (!branchDTO.getCityId().equals(userDTO.getPrimaryCity())) {
					throw new CustomWebServiceException("Primary Branch must be primary city's branch");
				}
			}
			if (!CollectionUtils.isEmpty(userDTO.getSecondaryCityList())) {
				Map<Long, String> cityMap = new HashMap<>(userDTO.getSecondaryCityList());
				cityMap.put(userDTO.getPrimaryCity(), userDTO.getPrimaryCityName());
				if (!CollectionUtils.isEmpty(userDTO.getSecondaryBranchList())) {
					List<Long> branchList = userDTO.getSecondaryBranchList().entrySet().stream().map(s -> s.getKey())
							.collect(Collectors.toList());
					List<BranchDTO> branchDTOList = branchService.findByIdIn(branchList);
					for (BranchDTO bdto : branchDTOList) {
						if (!cityMap.containsKey(bdto.getCityId())) {
							throw new CustomWebServiceException(
									"Secondary Branch must be primary city' branch or secondary city's branch");
						}
					}
				}
			}
			if(!userDTO.getPassword().equals(userDTO.getConfirmpassword())) {
				throw new CustomWebServiceException("Password is not match.");
			}
			userService.register(userDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
			redirectAttributes.addFlashAttribute("userDTO", userDTO);
			e.printStackTrace();
			return "redirect:create";
		}
		return "redirect:index?page=" + 1;
	}

	// add city in create form
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "add")
	public String addCity(@ModelAttribute("userDTO") UserDTO userDTO, RedirectAttributes redirectAttributes,
			Authentication auth) {

		if (!Objects.isNull(userDTO.getChooseCityId())) {
			try {
				CityDTO city = cityService.findById(userDTO.getChooseCityId());
				Map<Long, String> secondaryCityList = userDTO.getSecondaryCityList();
				secondaryCityList.put(city.getId(), city.getName());

				List<Long> cityIdList = secondaryCityList.entrySet().stream().map(s -> s.getKey())
						.collect(Collectors.toList());
				if (CollectionUtils.isEmpty(cityIdList))
					cityIdList = new ArrayList<>();
				cityIdList.add(userDTO.getPrimaryCity());
				List<BranchDTO> branchList = branchService.getBranchByCity(cityIdList);
				redirectAttributes.addFlashAttribute("branchList", branchList);
			} catch (CustomWebServiceException e) {
				// TODO Auto-generated catch block
				redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
			}
		}
		if (!Objects.isNull(userDTO.getChooseBranchId())) {
			try {
				BranchDTO branch = branchService.findById(userDTO.getChooseBranchId());
				Map<Long, String> secondaryBranchList = userDTO.getSecondaryBranchList();
				secondaryBranchList.put(branch.getId(), branch.getName());
			} catch (CustomWebServiceException e) {
				// TODO Auto-generated catch block
				redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
			}
		}
		// TODO Auto-generated catch block
		redirectAttributes.addFlashAttribute("userDTO", userDTO);
		return "redirect:create";
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public ModelAndView updateUser(@RequestParam("id") Long id, HttpSession session) {
		UserDTO userDTO = null;
		try {
			userDTO = userService.getUserById(id);
			
			List<Long> cityIdList = userDTO.getSecondaryCityList().entrySet().stream().map(s -> s.getKey())
					.collect(Collectors.toList());
			if (CollectionUtils.isEmpty(cityIdList))
				cityIdList = new ArrayList<>();
			cityIdList.add(userDTO.getPrimaryCity());
			List<BranchDTO> branchList = branchService.getBranchByCity(cityIdList);
			addViewObject("branchList", branchList);
			
			List<CityDTO> cityList = cityService.getAllCity();
			addViewObject("cityList", cityList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		addViewObject("userDTO", userDTO);
		addViewObject("statusList", Status.values());
		addViewObject("pageTitle", "User");
		return new ModelAndView("user_Form", getViewModelMap());
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String saveUpdate(@ModelAttribute("userDTO") UserDTO userDTO, RedirectAttributes redirectAttributes) {

		try {
			userService.update(userDTO, CommonUtil.getUsernameFromAuthentication());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
			e.printStackTrace();
			return "redirect:update?id=" + userDTO.getId();
		}
		return "redirect:index.do?page=" + 1;
	}

	@RequestMapping(value = "/getbranchbycity", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<BranchDTO> getwithPrimaryCity(@RequestParam("cityId") Long cityId, HttpSession session) {

		List<Long> cityIdList = new ArrayList<>();
		cityIdList.add(cityId);
		List<BranchDTO> result = branchService.getBranchByCity(cityIdList);
		return result;
	}
}
