package com.cbk.ep.controller;

import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cbk.ep.dto.CinemaDTO;
import com.cbk.ep.dto.CinemaInquiryDTO;
import com.cbk.ep.dto.CinemaPageDTO;
import com.cbk.ep.enums.Status;
import com.cbk.ep.service.CinemaService;
import com.cbk.ep.util.CommonUtil;

@Controller
@RequestMapping("/cinema")
public class CinemaController extends AbstractMvcController {

	@Autowired
	CinemaService cinemaService;

	// get all list
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView getlistData(@RequestParam("page") Integer page,
			@ModelAttribute("cinemaInquiryDTO") CinemaInquiryDTO cinemaInquiryDTO, HttpSession session,
			Authentication authentication) {
		if (this.containsKey("cinemaInquiryDTO")) {
			cinemaInquiryDTO = (CinemaInquiryDTO) this.getViewObject("cinemaInquiryDTO");
		} else
			cinemaInquiryDTO = new CinemaInquiryDTO();

		Pageable pageable = PageRequest.of(page - 1, CommonUtil.numberOfElements20);
		try {
			CinemaPageDTO cinemaPageDTO = cinemaService.findByFilter(cinemaInquiryDTO, pageable);
			this.addViewObject("cinemaList", cinemaPageDTO.getCinemaList());
			this.addViewObject("cinemaInquiryDTO", cinemaInquiryDTO);
			this.addViewObject("page", page);
			addViewObject("pageTitle", "Cinema List");
		} catch (Exception e) {

		}
		return new ModelAndView("cinema_List", this.getViewModelMap());
	}

	// search by inquiryDTO
	@RequestMapping(value = "/index", method = RequestMethod.POST)
	public String inquireByFilter(@ModelAttribute CinemaInquiryDTO cinemaInquiryDTO, HttpSession session) {
		this.addViewObject("cinemaInquiryDTO", cinemaInquiryDTO);
		// cinemaApplicationService.inquireCinemaByName(sessionContext, name);
		return "redirect:index?page=" + 1;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView createForm(HttpSession session, @ModelAttribute("cinemaDTO") CinemaDTO cinemaDTO) {
		if(Objects.isNull(cinemaDTO)) {
		cinemaDTO = new CinemaDTO();
		}
		
		addViewObject("cinemaDTO", cinemaDTO);
		addViewObject("statusList", Status.values());
		addViewObject("pageTitle", "Cinema");
		return new ModelAndView("cinema_Form", getViewModelMap());
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String saveCinema(@ModelAttribute("cinemaDTO") CinemaDTO cinemaDTO, RedirectAttributes redirectAttributes,
			Authentication auth) {

		try {
			cinemaService.register(cinemaDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
			redirectAttributes.addFlashAttribute("cinemaDTO", cinemaDTO);
			return "redirect:create";
		}
		return "redirect:index?page=" + 1;
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public ModelAndView updateCinema(@RequestParam("id") Long id, HttpSession session) {
		CinemaDTO cinemaDto = null;
		try {
			cinemaDto = cinemaService.findById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		addViewObject("cinemaDTO", cinemaDto);
		addViewObject("statusList", Status.values());
		addViewObject("pageTitle", "Cinema");
		return new ModelAndView("cinema_Form", getViewModelMap());
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String saveUpdate(@ModelAttribute("cinemaDTO") CinemaDTO cinemaDTO, HttpSession session) {

		try {
			cinemaService.update(cinemaDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:index.do?page=" + 1;
	}
}
