package com.cbk.ep.controller;

import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cbk.ep.dto.CityDTO;
import com.cbk.ep.dto.CityInquiryDTO;
import com.cbk.ep.dto.CityPageDTO;
import com.cbk.ep.enums.Status;
import com.cbk.ep.service.CityService;
import com.cbk.ep.util.CommonUtil;

@Controller
@RequestMapping("/city")
public class CityController extends AbstractMvcController {

	@Autowired
	CityService cityService;

	// get all list
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView getlistData(@RequestParam("page") Integer page,
			@ModelAttribute("cityInquiryDTO") CityInquiryDTO cityInquiryDTO, HttpSession session,
			Authentication authentication) {

		if (Objects.isNull(cityInquiryDTO)) {
			if (this.containsKey("cityInquiryDTO")) {
				cityInquiryDTO = (CityInquiryDTO) this.getViewObject("cityInquiryDTO");
			} else
				cityInquiryDTO = new CityInquiryDTO();
		}

		Pageable pageable = PageRequest.of(page - 1, CommonUtil.numberOfElements20);
		try {
			CityPageDTO cityPageDTO = cityService.findByFilter(cityInquiryDTO, pageable);
			this.addViewObject("cityList", cityPageDTO.getCityList());
			this.addViewObject("cityInquiryDTO", cityInquiryDTO);
			this.addViewObject("page", page);
			addViewObject("pageTitle", "City List");
		} catch (Exception e) {

		}
		return new ModelAndView("city_List", this.getViewModelMap());
	}

	// search by inquiryDTO
	@RequestMapping(value = "/index", method = RequestMethod.POST)
	public String inquireByFilter(@ModelAttribute("cityInquiryDTO") CityInquiryDTO cityInquiryDTO,
			HttpSession session) {
		this.addViewObject("cityInquiryDTO", cityInquiryDTO);
		// cinemaApplicationService.inquireCinemaByName(sessionContext, name);
		return "redirect:index?page=" + 1;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView createForm(HttpSession session) {
		CityDTO cityDTO = new CityDTO();
		addViewObject("cityDTO", cityDTO);
		addViewObject("statusList", Status.values());
		addViewObject("pageTitle", "City");
		return new ModelAndView("city_Form", getViewModelMap());
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String saveCinema(@ModelAttribute("cityDTO") CityDTO cityDTO, HttpSession session, Authentication auth) {

		try {
			cityService.register(cityDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:index?page=" + 1;
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public ModelAndView updateCinema(@RequestParam("id") Long id, HttpSession session) {
		CityDTO cityDTO = null;
		try {
			cityDTO = cityService.findById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		addViewObject("cityDTO", cityDTO);
		addViewObject("statusList", Status.values());
		addViewObject("pageTitle", "City");
		return new ModelAndView("city_Form", getViewModelMap());
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String saveUpdate(@ModelAttribute("cityDTO") CityDTO cityDTO, HttpSession session) {

		try {
			cityService.update(cityDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:index.do?page=" + 1;
	}
}
