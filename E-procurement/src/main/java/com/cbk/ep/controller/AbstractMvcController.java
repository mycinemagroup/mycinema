package com.cbk.ep.controller;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractMvcController {
	private Map<String, Object> viewModelMap = null;

	protected void addViewObject(String key, Object value) {
		if (viewModelMap == null) {
			viewModelMap = new HashMap<String, Object>();
		}
		viewModelMap.put(key, value);
	}

	protected void removeViewObject(String key) {
		if (viewModelMap != null) {
			viewModelMap.remove(key);
		}
	}

	protected Object getViewObject(String key) {
		Object object = null;
		if (viewModelMap != null) {
			object = viewModelMap.get(key);
		}
		return object;
	}

	protected void clearViewModelMap() {
		if (viewModelMap != null) {
			viewModelMap.clear();
		}
	}

	protected boolean containsKey(String key) {
		if (viewModelMap != null) {
			if (viewModelMap.containsKey(key)) {
				return true;
			} else
				return false;
		} else
			return false;
	}

	public Map<String, Object> getViewModelMap() {
		return viewModelMap;
	}
}
