package com.cbk.ep.controller;

import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cbk.ep.dto.FilmClassificationDTO;
import com.cbk.ep.dto.FilmClassificationInquiryDTO;
import com.cbk.ep.dto.FilmClassificationPageDTO;
import com.cbk.ep.enums.Status;
import com.cbk.ep.service.FilmClassificationService;
import com.cbk.ep.util.CommonUtil;

@Controller
@RequestMapping("/filmclassification")
public class FilmClassificationController extends AbstractMvcController {

	@Autowired
	FilmClassificationService filmClassificationService;

	// get all list
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView getlistData(@RequestParam("page") Integer page,
			@ModelAttribute("filmClassificationInquiryDTO") FilmClassificationInquiryDTO filmClassificationInquiryDTO,
			HttpSession session, Authentication authentication) {

		if (this.containsKey("filmClassificationInquiryDTO")) {
			filmClassificationInquiryDTO = (FilmClassificationInquiryDTO) this
					.getViewObject("filmClassificationInquiryDTO");
		} else
			filmClassificationInquiryDTO = new FilmClassificationInquiryDTO();

		Pageable pageable = PageRequest.of(page - 1, CommonUtil.numberOfElements20);
		try {
			FilmClassificationPageDTO filmClassificationPageDTO = filmClassificationService
					.findByFilter(filmClassificationInquiryDTO, pageable);
			this.addViewObject("filmClassificationList", filmClassificationPageDTO.getFilmClassificationList());
			this.addViewObject("filmClassificationPageDTO", filmClassificationPageDTO);
			this.addViewObject("page", page);
			this.addViewObject("pageTitle", "Film Classification List");
		} catch (Exception e) {
			this.addViewObject("errorMessage", e.getMessage());
		}
		return new ModelAndView("film_classification_List", this.getViewModelMap());
	}

	// search by inquiryDTO
	@RequestMapping(value = "/index", method = RequestMethod.POST)
	public String inquireByFilter(@ModelAttribute FilmClassificationInquiryDTO filmClassificationInquiryDTO,
			HttpSession session) {
		this.addViewObject("filmClassificationInquiryDTO", filmClassificationInquiryDTO);
		// cinemaApplicationService.inquireCinemaByName(sessionContext, name);
		return "redirect:index?page=" + 1;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView createForm(HttpSession session,
			@ModelAttribute("filmClassificationDTO") FilmClassificationDTO filmClassificationDTO) {

		if (Objects.isNull(filmClassificationDTO))
			filmClassificationDTO = new FilmClassificationDTO();
		addViewObject("filmClassificationDTO", filmClassificationDTO);
		addViewObject("statusList", Status.values());
		addViewObject("pageTitle", "Film Classification");
		return new ModelAndView("film_classification_Form", getViewModelMap());
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String saveBranch(@ModelAttribute("filmClassificationDTO") FilmClassificationDTO filmClassificationDTO,
			RedirectAttributes redirectAttributes, Authentication auth) {

		try {
			filmClassificationService.register(filmClassificationDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
			redirectAttributes.addFlashAttribute("filmClassificationDTO", filmClassificationDTO);
			e.printStackTrace();
			return "redirect:create";
		}
		return "redirect:index?page=" + 1;
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public ModelAndView updateBranch(@RequestParam("id") Long id, HttpSession session) {
		FilmClassificationDTO filmClassificationDTO = null;
		try {
			filmClassificationDTO = filmClassificationService.findById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		addViewObject("filmClassificationDTO", filmClassificationDTO);
		addViewObject("statusList", Status.values());
		addViewObject("pageTitle", "Film Classification");
		return new ModelAndView("film_classification_Form", getViewModelMap());
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String saveUpdate(@ModelAttribute("filmClassificationDTO") FilmClassificationDTO filmClassificationDTO,
			RedirectAttributes redirectAttributes) {

		try {
			filmClassificationService.update(filmClassificationDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
			e.printStackTrace();
			return "redirect:update?id=" + filmClassificationDTO.getId();
		}
		return "redirect:index.do?page=" + 1;
	}
}
