package com.cbk.ep.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.cbk.ep.enums.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "user")
public class User extends AbstractEntityObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "username", unique = true)
	private String username;

	@Getter(onMethod = @__(@JsonIgnore))
	@Setter
	@Column(name = "password")
	private String password;

	@OneToOne
	@JoinColumn(name = "role_id")
	private Role role;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private Status status;

	@Column(name = "email")
	private String email;

	@Column(name = "address")
	private String address;

	@Column(name = "phone_no")
	private String phoneNo;

	@Column(name = "primary_city")
	private Long primaryCity;

	@Column(name = "secondary_city")
	private String secondaryCity;

	@Column(name = "primary_branch")
	private Long primaryBranch;

	@Column(name = "secondary_branch")
	private String secondaryBranch;
}
