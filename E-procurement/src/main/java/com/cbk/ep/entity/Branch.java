package com.cbk.ep.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.cbk.ep.enums.Status;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "branch")
public class Branch extends AbstractEntityObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6670926190886141890L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "b2bphone")
	private String b2bphone;
	
	@Column(name = "hotline_phone1")
	private String hotlinePhone1;
	
	@Column(name = "hotline_phone2")
	private String hotlinePhone2;
	
	@Column(name = "hotline_phone3")
	private String hotlinePhone3;
	
	@Column(name = "photo_url")
	private String photoUrl;
	
	@ManyToOne
	@JoinColumn(name = "city_id")
	private City city;
	
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status;
}
