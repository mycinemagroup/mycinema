package com.cbk.ep.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.cbk.ep.enums.Status;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "cinema")
public class Cinema implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8055755665036916269L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "phno")
	private String phno;
	
	@Column(name = "photo_url")
	private String photoUrl;
	
	@Column(name = "details")
	private String details;
	
	@Column(name = "no_of_room")
	private String noOfRoom;
	
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@Column(name = "admin_json")
	private String adminJson;
	
	@Column(name = "firebase_initialize_app_config")
	private String firebaseInitializeAppConfig;
	
	@Column(name = "database_url")
	private String databaseURL;
	
	@Column(name = "created_by")
	private Long createdBy;

	@Column(name = "creation_date")
	private Date creationDate;

	@Column(name = "modified_by")
	private Long modifiedBy;

	@Column(name = "modified_date")
	private Date modifiedDate;
}
