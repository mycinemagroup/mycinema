package com.cbk.ep.entity;

import com.cbk.ep.enums.Status;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Genre.class)
public abstract class Genre_ extends com.cbk.ep.entity.AbstractEntityObject_ {

	public static volatile SingularAttribute<Genre, String> name;
	public static volatile SingularAttribute<Genre, String> description;
	public static volatile SingularAttribute<Genre, Long> id;
	public static volatile SingularAttribute<Genre, Status> status;

	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String STATUS = "status";

}

